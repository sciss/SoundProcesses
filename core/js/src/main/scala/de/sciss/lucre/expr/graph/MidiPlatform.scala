/*
 *  MidiPlatform.scala
 *  (SoundProcesses)
 *
 *  Copyright (c) 2010-2025 Hanns Holger Rutz. All rights reserved.
 *
 *	This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.lucre.expr.graph

import de.sciss.lucre.impl.IDummyEvent
import de.sciss.lucre.{Cursor, IEvent, ITargets, Txn}

trait MidiPlatform extends MidiBase {
  type PeerDevice   = Nothing
  type PeerMessage  = Nothing

  protected def listDevices(): Seq[Device] = Nil

  protected def mkShortMessage(command: Int, channel: Int, data1: Int, data2: Int): PeerMessage =
    throw new UnsupportedOperationException()

  protected def mkSysexMessage(data: Array[Byte]): PeerMessage =
    throw new UnsupportedOperationException()

  protected def matchShortMessage(m: PeerMessage): Option[(Int, Int, Int, Int)] = None

  protected final class InExpanded[T <: Txn[T]](dev: Device)(implicit protected val targets: ITargets[T],
                                                             cursor: Cursor[T]) extends InRepr[T] {

    override def received: IEvent[T, PeerMessage] = IDummyEvent[T, PeerMessage]()

    override def initControl()(implicit tx: T): Unit = ()

    override def dispose()(implicit tx: T): Unit = ()
  }

  protected final class OutExpanded[T <: Txn[T]](dev: Device) extends OutRepr[T] {
    override def send(m: Nothing)(implicit tx: T): Unit = ()

    override def initControl()(implicit tx: T): Unit = ()

    override def dispose()(implicit tx: T): Unit = ()
  }
}
