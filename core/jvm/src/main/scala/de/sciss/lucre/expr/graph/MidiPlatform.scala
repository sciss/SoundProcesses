/*
 *  MidiPlatform.scala
 *  (SoundProcesses)
 *
 *  Copyright (c) 2010-2025 Hanns Holger Rutz. All rights reserved.
 *
 *	This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.lucre.expr.graph

import de.sciss.lucre.impl.IGeneratorEvent
import de.sciss.lucre.{Cursor, IEvent, IPull, ITargets, Txn}
import de.sciss.proc.SoundProcesses

import javax.sound.{midi => jmidi}
import scala.concurrent.stm.Ref
import scala.util.control.NonFatal

trait MidiPlatform extends MidiBase {
  type PeerDevice   = jmidi.MidiDevice
  type PeerMessage  = jmidi.MidiMessage

  protected def listDevices(): Seq[Device] = {
    val infoArr = jmidi.MidiSystem.getMidiDeviceInfo
    val b = Seq.newBuilder[Device]
    b.sizeHint(infoArr.length)
    var i = 0
    while (i < infoArr.length) {
      val info = infoArr(i)
      try {
        val peer = jmidi.MidiSystem.getMidiDevice(info)
        val dev = new DeviceImpl(peer)
        b += dev
      } catch {
        case NonFatal(_) =>
      }
      i += 1
    }
    b.result()
  }

  protected def mkShortMessage(command: Int, channel: Int, data1: Int, data2: Int): PeerMessage =
    new jmidi.ShortMessage(command, channel, data1, data2)

  protected def mkSysexMessage(data: Array[Byte]): PeerMessage =
    new jmidi.SysexMessage(data, data.length)

  protected def matchShortMessage(m: PeerMessage): Option[(Int, Int, Int, Int)] = m match {
    case j: jmidi.ShortMessage => Some((j.getCommand, j.getChannel, j.getData1, j.getData2))
    case _ => None
  }

  private final class DeviceImpl(override val peer: PeerDevice) extends Device {
    override def name   : String = peer.getDeviceInfo.getName
    override def descr  : String = peer.getDeviceInfo.getDescription
    override def vendor : String = peer.getDeviceInfo.getVendor

    override def isInput  : Boolean = peer.getMaxTransmitters != 0
    override def isOutput : Boolean = peer.getMaxReceivers    != 0

    override def toString: String = s"Device($name)@${hashCode().toHexString}"
  }

  protected final class InExpanded[T <: Txn[T]](dev: Device)
                                           (implicit protected val targets: ITargets[T],
                                            cursor: Cursor[T])
    extends InRepr[T] with IGeneratorEvent[T, jmidi.MidiMessage] {

    private[this] val tRef = Ref.make[jmidi.Transmitter]()
    private[this] val isReal = dev.name.nonEmpty

    override private[lucre] def pullUpdate(pull: IPull[T])(implicit tx: T): Option[jmidi.MidiMessage] =
      Some(pull.resolve[jmidi.MidiMessage])

    override def received: IEvent[T, jmidi.MidiMessage] = this

    override def initControl()(implicit tx: T): Unit =
      if (isReal) tx.afterCommit {
        val peer = dev.peer
        peer.open()
        val t = peer.getTransmitter
        t.setReceiver(new jmidi.Receiver {
          // XXX TODO should we pay attention to the `timeStamp`
          override def send(m: jmidi.MidiMessage, timeStamp: Long): Unit =
            SoundProcesses.step[T]("rcv") { implicit tx =>
              fire(m)
            }

          override def close(): Unit = ()
        })
        tRef.single.set(t)
      }

    override def dispose()(implicit tx: T): Unit =
      if (isReal) {
        val t = tRef.swap(null)(tx.peer)
        tx.afterCommit {
          dev.peer.close()
          if (t != null) t.close()
        }
      }
  }


  protected final class OutExpanded[T <: Txn[T]](dev: Device) extends OutRepr[T] {

    private[this] val rRef    = Ref.make[jmidi.Receiver]()
    private[this] val isReal  = dev.name.nonEmpty

    override def send(m: jmidi.MidiMessage)(implicit tx: T): Unit = {
      if (isReal) tx.afterCommit {
        val r = rRef.single.get
        if (r != null) r.send(m, -1L)  // XXX TODO time stamping?
      }
    }

    override def initControl()(implicit tx: T): Unit =
      if (isReal) tx.afterCommit {
        val peer = dev.peer
        peer.open()
        val r = peer.getReceiver
        rRef.single.set(r)
      }

    override def dispose()(implicit tx: T): Unit =
      if (isReal) {
        val r = rRef.swap(null)(tx.peer)
        tx.afterCommit {
          dev.peer.close()
          if (r != null) r.close()
        }
      }
  }
}
