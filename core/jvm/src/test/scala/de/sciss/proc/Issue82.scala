package de.sciss.proc

import de.sciss.lucre.expr.graph
import de.sciss.span.Span
import de.sciss.synth
import de.sciss.synth.Client

import scala.concurrent.Promise

/*

testOnly de.sciss.proc.Issue82

 */
class Issue82 extends BounceSpec {
  override def sampleRate: Double = 48000

  "A Timeline with synth can be started from a Runner" works { implicit universe =>
    import universe.cursor

    val cH = cursor.step { implicit tx =>
      import Implicits._
      val _p1 = mkProc {
        import synth._
        import Import._
        import proc.graph.Ops._
        import proc.graph._
        import ugen._
        val freq = "freq".kr(441)
        freq.poll(0, "issue82 freq")
        val tr = TDelay.kr(Impulse.kr(0), dur = 0.1f)
        Action(tr, "done")
        PhysicalOut(0, SinOsc.ar(freq) * 0.1)
        ()
      }
      _p1.name = "sine"

      val tl = Timeline[T]()
      tl.add(Span(1.0.seconds, 4.0.seconds), _p1)

      val _c = Control[T]()
      _c.graph() = Control.Graph {
        import ExImport._
        import graph._
        val r     = Runner("proc")
        val lb    = LoadBang()
        val done  = Act(
          // PrintLn("Issue82 Done"),
          ThisRunner().done,
        )
        val st    = r.state
        st.changed --> PrintLn("STATE = " ++ st.toStr)
        lb --> Act(
          // PrintLn("Issue82 Begin"),
          r.runWith("freq" -> 600, "done" -> done)
        )
      }

      _c.attr.put("proc", tl)

      // tx.newHandle(tl)
      tx.newHandle(_c)
    }

    val pStatus = Promise[Unit]()

    def runCtl(r: Runner[T])(implicit tx: T): Unit = {
      // r.stop()
      r.run()
      r.reactNow { implicit tx => {
        case Runner.Done        => tx.afterCommit { pStatus.success(()) }
        case Runner.Failed(ex)  => tx.afterCommit { pStatus.tryFailure(ex); () }
        case _                  => ()
      }}
      ()
    }

    // SoundProcesses.logAural.level = Level.All

    val r = cursor.step { implicit tx =>
      val _c = cH()
      Runner(_c)
    }

    val client = Client.Config()
    // client.latency
    val res = runServer(client = client) { s =>
      r.universe.scheduler.stepTag { implicit tx =>
        runCtl(r)
        pStatus.future.andThen {
          case _ => s.peer.quit()
        }
      }
    }

    res.map { _ => assert(true) }
  }
}