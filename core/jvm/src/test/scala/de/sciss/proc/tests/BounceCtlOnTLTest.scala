package de.sciss.proc.tests

import de.sciss.lucre.expr.{Graph, graph}
import de.sciss.lucre.synth.{InMemory, Server, Txn}
import de.sciss.lucre.{Obj, Source}
import de.sciss.proc.{Bounce, Control, Proc, TimeRef, Timeline, Universe}
import de.sciss.processor.Processor
import de.sciss.span.Span
import de.sciss.synth.{Client, SynthGraph}
import de.sciss.file._

import scala.concurrent.ExecutionContext

object BounceCtlOnTLTest extends App {
  type S = InMemory
  type T = InMemory.Txn

  val gProc = SynthGraph {
    import de.sciss.synth.GEOps._
    import de.sciss.synth.ugen._
    val f = IRand(70, 100).midiCps
    val s = SinOsc.ar(f) * EnvGen.ar(Env.perc, levelScale = 0.1)
    PhysicalOut.ar(0, s)
  }

  val gCtl = Graph {
    import graph._
    val r = Runner("proc")
    val lb = LoadBang()

    val actPlay = Act(
      PrintLn("play"),
      r.stop,
      r.run
    )

    val actNext = Act.link { self =>
      Act(
        actPlay,
        Delay(0.5)(self)
      )
    }

    lb --> actNext
  }

  implicit val system: S = InMemory()

  final case class PerformSettings[T <: Txn[T]](
                                                 source      : Source[T, Obj[T]],
                                                 server      : Server.Config,
                                                 client      : Client.Config,
                                                 span        : Span,
                                               )
  implicit val executionContext: ExecutionContext = ExecutionContext.global

  def performBounce[T <: Txn[T]](settings: PerformSettings[T])
                          (implicit universe: Universe[T]): Processor[File] = {

    // for real-time, we generally have to overshoot because in SC 3.6, DiskOut's
    // buffer is not flushed after synth is stopped.
    val numServerChannels = settings.server.outputBusChannels
    val numFileChannels   = 1
    assert (numFileChannels <= numServerChannels, s"numFileChannels $numFileChannels, numServerChannels $numServerChannels")

    val span              = settings.span
    val fileOut           = new File(settings.server.nrtOutputPath)
    val sampleRate        = settings.server.sampleRate
    val fileFrames0       = (span.length * sampleRate / TimeRef.SampleRate + 0.5).toLong
    val fileFrames        = fileFrames0 // - (fileFrames0 % settings.server.blockSize)

    val settings1: PerformSettings[T] = settings

    val bounce    = Bounce[T]() // workspace.I
    val bnc       = Bounce.Config[T]()
    bnc.group     = collection.immutable.Seq(settings1.source)
    bnc.realtime  = false
    bnc.server.read(settings1.server)
    bnc.client.read(settings1.client)

    val span1 = span
    bnc.span    = span1

    // XXX TODO --- could use filtered console output via Poll to
    // measure max gain already during bounce
    val bProcess  = bounce.apply(bnc)
    // bProcess.addListener {
    //   case u => println(s"UPDATE: $u")
    // }
    bProcess.start()
    val process = bProcess
    process
  }

  val pathOut = (userHome / "Downloads" / "test-bounce.aif").path // File.createTemp("bounce", ".aif").getPath

  val pSet = system.step { implicit tx =>
//    implicit val u    : Universe    [T] = Universe.dummy
//    implicit val undo : UndoManager [T] = UndoManager()
//    implicit val ctx  : Context     [T] = ExprContext()
//    gCtl.expand[T].initControl()

    val c = Control[T]()
    c.graph() = gCtl
    val p = Proc[T]()
    p.graph() = gProc
    c.attr.put("proc", p)
    val tl = Timeline[T]()
    val sp = Span((TimeRef.SampleRate * 1).toLong, (TimeRef.SampleRate * 4).toLong)
    tl.add(sp, c)
//    tl.add(sp, p)

    val sConfig = Server.Config()
    val cConfig = Client.Config()

    sConfig.nrtOutputPath      = pathOut
    sConfig.sampleRate         = 44100
    sConfig.outputBusChannels  = 1

    PerformSettings(
      source  = tx.newHandle(tl),
      server  = sConfig,
      client  = cConfig,
      span    = sp,
    )
  }

  implicit val u: Universe[T] = system.step { implicit tx => Universe.dummy[T] }
  val process = performBounce(pSet)

  val t: Thread = new Thread {
    override def run(): Unit = {
      this.synchronized(this.wait())
      sys.exit(0)
    }
  }
  t.start()

  var lastProgress = 0
  process.addListener {
    case progress @ Processor.Progress(_, _) =>
      val p = progress.toInt
      while (lastProgress < p) {
        print('#')
        lastProgress += 2
      }

    case Processor.Result(_, res) =>
      println(s" $lastProgress%")
      println(res)
      t.synchronized(t.notifyAll())
  }
//  process.start()
}
