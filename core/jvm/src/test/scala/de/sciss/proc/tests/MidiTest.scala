package de.sciss.proc.tests

import de.sciss.lucre.edit.UndoManager
import de.sciss.lucre.expr.Context
import de.sciss.lucre.synth.InMemory
import de.sciss.proc
import de.sciss.proc.{Control, ExprContext, SoundProcesses, Universe}

object MidiTest {
  def main(args: Array[String]): Unit = {
    run()
  }

  type S = InMemory
  type T = InMemory.Txn

  def run(): Unit = {
    SoundProcesses.init()
    // val m     = Midi.In("EC4*")
    implicit val sys: S = InMemory()
    sys.step { implicit tx =>
      val c = Control[T]()
      c.graph() = Control.Graph {
        import de.sciss.lucre.expr.ExImport._
        import de.sciss.lucre.expr.graph._

        val devs = Midi.Devices()
        val devOpt = devs.find { d => d.name.contains("EC4") && d.isInput }
        val dev = devOpt.getOrElse(Midi.Device.Empty())
        val m = Midi.In(dev)

        LoadBang() --> PrintLn("INIT. device found? " ++ devOpt.isDefined.toStr)
        LoadBang() --> devs.map(x => PrintLn(x.toStr))

        val vrVal = Var(0)
        val vrNum = Var(0)
        m.cc(chan = Quote(0), num = vrNum /*Quote(100)*/, value = vrVal) -->
          PrintLn("CC " ++ vrNum.toStr ++ " has value " ++ vrVal.toStr)

        Runner("bla").runWith("cc" -> (vrVal / 127.0))
      }

      implicit val u: Universe[T] = Universe.dummy[T]
//      val r = proc.Runner(c)
//      r.run()
      implicit val undo: UndoManager[T] = UndoManager()
      implicit val ctx: Context[T] = ExprContext()
      val g = c.graph.value
      val res = g.expand[T]
//      val res = Try(expandGraph())
//      addDisposable(ctx)
//      ctlRef() = Some(res)
      res.initControl()
    }
  }
}
