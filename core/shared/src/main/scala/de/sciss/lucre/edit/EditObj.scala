/*
 *  EditObj.scala
 *  (SoundProcesses)
 *
 *  Copyright (c) 2010-2025 Hanns Holger Rutz. All rights reserved.
 *
 *	This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.lucre.edit

import de.sciss.equal.Implicits._
import de.sciss.lucre.{Copy, Expr, Folder, Obj, Txn}
import de.sciss.proc.Proc

object EditObj {
  /** Makes a "shallow" copy of an object.
    * If the object is an expression variable, puts its contents into a fresh variable.
    * If the object is an expression constant, puts its contents into a fresh variable.
    * Otherwise just returns the input without making a copy.
    */
  def shallowCopy[T <: Txn[T]](in: Obj[T])(implicit tx: T /*, context: Copy[T, T]*/): Obj[T] =
    in match {
      case ex: Expr[T, _] =>
        ex.tpe match {
          case tpe: Expr.Type[_, _] =>
            val exT = ex.asInstanceOf[tpe.E[T]] // XXX TODO what else can we do
            tpe.Var.unapply(exT) match {
              case Some(vr) =>
                val exVl = vr()
                tpe.newVar[T](exVl).asInstanceOf[Obj[T]]
              case None => exT match {
                case _: Expr.Const[T, _] =>
                  val exVl = exT
                  tpe.newVar[T](exVl).asInstanceOf[Obj[T]]
                case _ => ex
              }
            }

          case _ =>
            ex
        }

      case _ => in
    }

  /** Makes a copy of an object, including shallow attributes.
    * If `connect` is false, does not put `Proc.mainIn`.
    *
    * Does not produce any undoable edits.
    *
    * @param in  the process to copy
    */
  def copyDo[T <: Txn[T]](in: Obj[T], connectInput: Boolean)(implicit tx: T): Obj[T] = {
    implicit val context: Copy[T, T] = Copy()
    val out = context.copyPlain(in)
    tx.attrMapOption(in).foreach { attrIn =>
      val attrOut = out.attr
      attrIn.iterator.foreach { case (key, valueIn) =>
        if (key !== Proc.mainIn) {
          val valueOut = shallowCopy(valueIn)
          attrOut.put(key, valueOut)
        } else if (connectInput) {
          val valueOpt = attrIn.get(Proc.mainIn).collect {
            case op: Proc.Output[T] => op
            case fIn: Folder[T] =>
              val fOut = Folder[T]()
              fIn.iterator.foreach { op => fOut.addLast(op) }
              fOut
          }
          valueOpt.foreach(value => attrOut.put(Proc.mainIn, value))
        }
      }
    }
    context.finish()
    out
  }
}
