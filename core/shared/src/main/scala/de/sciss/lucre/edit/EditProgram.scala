/*
 *  EditProgram.scala
 *  (SoundProcesses)
 *
 *  Copyright (c) 2010-2025 Hanns Holger Rutz. All rights reserved.
 *
 *	This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.lucre.edit

import de.sciss.lucre.edit.impl.BasicUndoableEdit
import de.sciss.lucre.expr.graph.Ex
import de.sciss.lucre.{Expr, Source, Txn}

object EditProgram {
  def apply[T <: Txn[T], A, Repr[~ <: Txn[~]] <: Expr[~, A]](name: String, tpe: Expr.Type[A, Repr])
                                                            (ex: tpe.Program[T], value: Ex[A])
                                                            (implicit tx: T, undoManager: UndoManager[T]): Unit = {
    import tpe.programFormat
    val exH     = tx.newHandle(ex)
    val before  = ex.program()
    val now     = value
    val res = new Impl[T, A](name, exH = exH, before = before, now = now)
    res.perform()
    undoManager.addEdit(res)
  }

  private final class Impl[T <: Txn[T], A](val name: String, exH: Source[T, Expr.Program[T, A]],
                                           before: Ex[A], now: Ex[A])
    extends BasicUndoableEdit[T] {

    override protected def undoImpl()(implicit tx: T): Unit = {
      val ex = exH()
      ex.program() = before
    }

    override protected def redoImpl()(implicit tx: T): Unit =
      perform()

    def perform()(implicit tx: T): Unit = {
      val ex = exH()
      ex.program() = now
    }
  }
}