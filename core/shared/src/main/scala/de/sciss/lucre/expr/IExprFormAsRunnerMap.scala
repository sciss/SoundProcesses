/*
 *  IExprFormAsRunnerMap.scala
 *  (SoundProcesses)
 *
 *  Copyright (c) 2010-2025 Hanns Holger Rutz. All rights reserved.
 *
 *	This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.lucre
package expr

import de.sciss.lucre.Txn.peer

import scala.concurrent.stm.TMap

final class IExprFormAsRunnerMap[T <: Txn[T]](pairs: Seq[(IExpr[T, String], Form[T])], tx0: T)
                                         (implicit targets: ITargets[T])
  extends MapObjLike[T, String, Form[T]] {

  private[this] val map: TMap[String, Form[T]] = TMap({
    pairs.map { case (k, v) =>
      val kv = k.value(tx0)
      (kv, v)
    }
  }: _*)

  def isEmpty(implicit tx: T): Boolean =
    map.isEmpty

  def nonEmpty(implicit tx: T): Boolean =
    map.nonEmpty

  // XXX TODO --- we currently don't update `map`
  def changed: Observable[T, MapObjLike.Update[String, Form[T]]] = Observable.empty

  def contains(key: String)(implicit tx: T): Boolean =
    map.contains(key)

  def get(key: String)(implicit tx: T): Option[Form[T]] =
    map.get(key)

  def dispose()(implicit tx: T): Unit = {
    map.clear()
  }
}
