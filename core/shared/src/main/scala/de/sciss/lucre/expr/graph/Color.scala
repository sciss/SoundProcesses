/*
 *  Color.scala
 *  (SoundProcesses)
 *
 *  Copyright (c) 2010-2025 Hanns Holger Rutz. All rights reserved.
 *
 *	This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.lucre.expr.graph

import de.sciss.lucre.Adjunct.{FromAny, HasDefault}
import de.sciss.lucre.expr.Context
import de.sciss.lucre.expr.ExElem.{ProductReader, RefMapIn}
import de.sciss.lucre.expr.graph.impl.MappedIExpr
import de.sciss.lucre.expr.impl.BasicExObjBridgeImpl
import de.sciss.lucre.{Adjunct, IExpr, ITargets, Txn}
import de.sciss.numbers.Implicits._
import de.sciss.numbers.IntFunctions
import de.sciss.proc.Color.Palette
import de.sciss.proc.ExImport.Color
import de.sciss.proc.{Color => _Color}
import de.sciss.serial.DataInput

import scala.annotation.switch

object Color extends ProductReader[Ex[Color]] {
  private lazy val _init: Unit =
    Adjunct.addFactory(TypeImpl)

  def init(): Unit = _init

  def DarkBlue    : Ex[Color]  = Const(Palette( 0))
  def LightBlue   : Ex[Color]  = Const(Palette( 1))
  def Cyan        : Ex[Color]  = Const(Palette( 2))
  def Mint        : Ex[Color]  = Const(Palette( 3))
  def Green       : Ex[Color]  = Const(Palette( 4))
  def Yellow      : Ex[Color]  = Const(Palette( 5))
  def DarkBeige   : Ex[Color]  = Const(Palette( 6))
  def LightBeige  : Ex[Color]  = Const(Palette( 7))
  def Orange      : Ex[Color]  = Const(Palette( 8))
  def Red         : Ex[Color]  = Const(Palette( 9))
  def Maroon      : Ex[Color]  = Const(Palette(10))
  def Fuchsia     : Ex[Color]  = Const(Palette(11))
  def Purple      : Ex[Color]  = Const(Palette(12))
  def Black       : Ex[Color]  = Const(Palette(13))
  def Silver      : Ex[Color]  = Const(Palette(14))
  def White       : Ex[Color]  = Const(Palette(15))

  def Type: Obj.Bridge[Color] with Obj.CanMake[Color] with HasDefault[Color] with FromAny[Color] = TypeImpl

  private object TypeImpl extends BasicExObjBridgeImpl[Color, _Color.Obj](_Color.Obj)
    with Ex.Value[Color] with FromAny[Color]
    with HasDefault[Color] with Adjunct.Factory {

    override def toString: String = "Color"

    import _Color.{Obj => tpe}

    final val id = 2009

    // WARNING: this must correspond with the serialization of `AbstractExObjBridgeImpl`!
    override def readIdentifiedAdjunct(in: DataInput): Adjunct = {
      val typeId = in.readInt()
      assert (typeId == tpe.typeId)
      this

    }
    override def defaultValue: Color = _Color.Black

    override def fromAny(in: Any): Option[Color] = in match {
      case c: Color => Some(c)
      case _        => None
    }
  }

  object Predef extends ProductReader[Ex[Color]] {
    /** There are sixteen predefined colors (identifiers 0 to 15). */
    def apply(id: Ex[Int]): Ex[Color] = Impl(id)

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Ex[Color] = {
      require (arity == 1 && adj == 0)
      val _id = in.readEx[Int]()
      Predef(_id)
    }

    private final class Expanded[T <: Txn[T]](id: IExpr[T, Int], tx0: T)(implicit targets: ITargets[T])
      extends MappedIExpr[T, Int, Color](id, tx0) {

      protected def mapValue(inValue: Int)(implicit tx: T): Color = {
        val idx = IntFunctions.mod(inValue, Palette.size)
        Palette(idx)
      }
    }

    private final case class Impl(id: Ex[Int]) extends Ex[Color] {
      type Repr[T <: Txn[T]] = IExpr[T, Color]

      override def productPrefix: String = s"Color$$Predef" // serialization

      protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
        import ctx.targets
        new Expanded(id.expand[T], tx)
      }
    }
  }
Double
  def apply(argb: Ex[Int] = 0): Ex[Color] = Apply(argb)

  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Ex[Color] = {
    require(arity == 1 && adj == 0)
    val _argb = in.readEx[Int]()
    Apply(_argb)
  }

  // note: this only used in expansion and never serialized
  private final case class ApplyOp() extends UnaryOp.Op[Int, Color] {
    override def productPrefix: String = "Color" // serialization

    override def apply(argb: Int): Color = _Color.User(argb)
  }

  private final case class Apply(argb: Ex[Int]) extends Ex[Color] {
    override def productPrefix: String = "Color" // serialization

    type Repr[T <: Txn[T]] = IExpr[T, Color]

    protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
      import ctx.targets
      new UnaryOp.Expanded(ApplyOp(), argb.expand[T], tx)
    }
  }

  // note: this only used in expansion and never serialized
  private final case class FromHSBOp() extends TernaryOp.Op[Double, Double, Double, Color] {
    override def productPrefix: String = s"Color$$FromHSBOp" // serialization

    override def apply(hue: Double, sat: Double, bri: Double): Color = {
      val rgb   = HsbToRgb(hue, sat, bri)
      val argb  = 0xFF000000 | rgb
      _Color.User(argb)
    }
  }

  object FromHSB extends ProductReader[FromHSB] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): FromHSB = {
      require (arity == 3 && adj == 0)
      val _hue = in.readEx[Double]()
      val _sat = in.readEx[Double]()
      val _bri = in.readEx[Double]()
      FromHSB(_hue, _sat, _bri)
    }
  }
  final case class FromHSB(hue: Ex[Double], saturation: Ex[Double], brightness: Ex[Double]) extends Ex[Color] {
    override def productPrefix: String = s"Color$$FromHSB" // serialization

    type Repr[T <: Txn[T]] = IExpr[T, Color]

    protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
      import ctx.targets
      new TernaryOp.Expanded(FromHSBOp(), hue.expand[T], saturation.expand[T], brightness.expand[T], tx)
    }
  }

  // note: this only used in expansion and never serialized
  private final case class ARGBOp() extends UnaryOp.Op[Color, Int] {
    override def productPrefix: String = s"Color$$ARGBOp" // serialization

    override def apply(c: Color): Int = c.argb
  }

  object ARGB extends ProductReader[ARGB] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): ARGB = {
      require (arity == 1 && adj == 0)
      val _in = in.readEx[Color]()
      ARGB(_in)
    }
  }
  final case class ARGB(in: Ex[Color]) extends Ex[Int] {
    override def productPrefix: String = s"Color$$ARGB" // serialization

    type Repr[T <: Txn[T]] = IExpr[T, Int]

    protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
      import ctx.targets
      new UnaryOp.Expanded(ARGBOp(), in.expand[T], tx)
    }
  }

  // note: this only used in expansion and never serialized
  private final case class ComponentOp() extends BinaryOp.Op[Color, Int, Double] {
    override def productPrefix: String = s"Color$$ComponentOp" // serialization

    override def apply(c: Color, i: Int): Double = {
      val x   = c.argb
      val r   = (x >>> 16) & 0xFF
      val g   = (x >>>  8) & 0xFF
      val b   =  x         & 0xFF
      val ic  = i.clip(0, 7)
      (ic: @switch) match {
        case 0 => ((x >>> 24) & 0xFF) / 255.0 // alpha
        case 1 => r / 255.0 // red
        case 2 => g / 255.0 // green
        case 3 => b / 255.0 // blue
        case 7 => (0.2126 * r + 0.7152 * g + 0.0722 * b) / 255.0  // luminance
        case _ =>
          val cMax  = math.max(r, math.max(g, b))
          if (ic == 6) cMax / 255.0 // brightness
          else {
            val cMin = math.min(r, math.min(g, b))
            val sat  = if (cMax == 0) 0.0 else (cMax - cMin).toDouble / cMax
            if (ic == 5) sat // saturation
            else {  // hue
              if (sat == 0.0) 0.0 else {
                val cSpan = (cMax - cMin).toDouble
                val rc    = cMax - r
                val gc    = cMax - g
                val bc    = cMax - b
                val hue0  =
                  if      (r == cMax)       (bc - gc) / cSpan
                  else if (g == cMax) 2.0 + (rc - bc) / cSpan
                  else                4.0 + (gc - rc) / cSpan
                val hue1  = hue0 / 6.0
                if (hue1 >= 0) hue1 else hue1 + 1.0
              }
            }
          }
      }
    }
  }

  object Component extends ProductReader[Component] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Component = {
      require (arity == 2 && adj == 0)
      val _in   = in.readEx[_Color]()
      val _tpe  = in.readEx[Int]()
      Component(_in, _tpe)
    }
  }
  /** Extracts a component of the color.
    *
    * @param tpe  0 for alpha, 1-3 for red, green, blue; 4-6 for hue, saturation, brightness, 7 for luminance
    */
  final case class Component(in: Ex[Color], tpe: Ex[Int]) extends Ex[Double] {
    override def productPrefix: String = s"Color$$Component" // serialization

    type Repr[T <: Txn[T]] = IExpr[T, Double]

    protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
      import ctx.targets
      new BinaryOp.Expanded(ComponentOp(), in.expand[T], tpe.expand[T], tx)
    }
  }

  private def RgbToHsb(r: Int, g: Int, b: Int): (Double, Double, Double) = {
    val cMax  = math.max(r, math.max(g, b))
    val cMin  = math.min(r, math.min(g, b))
    val bri   = cMax / 255.0
    val sat   = if (cMax == 0) 0.0 else (cMax - cMin).toDouble / cMax
    val hue   = if (sat == 0.0) 0.0 else {
      val cSpan = (cMax - cMin).toDouble
      val rc    = cMax - r
      val gc    = cMax - g
      val bc    = cMax - b
      val hue0  =
        if      (r == cMax)       (bc - gc) / cSpan
        else if (g == cMax) 2.0 + (rc - bc) / cSpan
        else                4.0 + (gc - rc) / cSpan
      val hue1  = hue0 / 6.0
      if (hue1 >= 0) hue1 else hue1 + 1.0
    }
    (hue, sat, bri)
  }

  // assumes brightness and saturation values are clipped. does not set alpha component
  private def HsbToRgb(hue: Double, sat: Double, bri: Double): Int = {
    var r = 0.0
    var g = 0.0
    var b = 0.0
    if (sat <= 0) {
      r = bri; g = bri; b = bri
    } else {
      val hueM  = (hue - math.floor(hue)) * 6.0
      val frac  = hueM - math.floor(hueM)
      val x     = bri * (1.0 - sat)
      val y     = bri * (1.0 - sat * frac)
      val z     = bri * (1.0 - (sat * (1.0 - frac)))
      (hueM.toInt: @switch) match {
        case 0 => r = bri; g = z  ; b = x
        case 1 => r = y  ; g = bri; b = x
        case 2 => r = x  ; g = bri; b = z
        case 3 => r = x  ; g = y  ; b = bri
        case 4 => r = z  ; g = x  ; b = bri
        case 5 => r = bri; g = x  ; b = y
      }
    }
    val ri = (r * 255 + 0.5).toInt
    val gi = (g * 255 + 0.5).toInt
    val bi = (b * 255 + 0.5).toInt
    (ri << 16) | (gi << 8) | (bi << 0)
  }

  // note: this only used in expansion and never serialized
  private final case class MixOp() extends QuaternaryOp.Op[Color, Color, Double, Int, Color] {
    override def productPrefix: String = s"Color$$MixOp" // serialization

    override def apply(c1: Color, c2: Color, w2: Double, mode: Int): Color = {
      if      (w2 <= 0.0) return c1
      else if (w2 >= 1.0) return c2

      val x   = c1.argb
      val a1  = (x >>> 24) & 0xFF
      val r1  = (x >>> 16) & 0xFF
      val g1  = (x >>>  8) & 0xFF
      val b1  =  x         & 0xFF
      val y   = c2.argb
      val a2  = (y >>> 24) & 0xFF
      val r2  = (y >>> 16) & 0xFF
      val g2  = (y >>>  8) & 0xFF
      val b2  =  y         & 0xFF
      val w1  = 1.0 - w2
      val a   = (a1 * w1 + a2 * w2 + 0.5).toInt
      val argb = if (mode <= 0) {  // RGB
        val r     = (r1 * w1 + r2 * w2 + 0.5).toInt
        val g     = (g1 * w1 + g2 * w2 + 0.5).toInt
        val b     = (b1 * w1 + b2 * w2 + 0.5).toInt
        (a << 24) | (r << 16) | (g << 8) | b
      } else { // HSB
        val (hue1, sat1, bri1) = RgbToHsb(r1, g1, b1)
        val (hue2, sat2, bri2) = RgbToHsb(r2, g2, b2)
        val hueClose = (hue1 absDif hue2) <= 0.5
        val hue1M = if (hueClose || hue1 > hue2) hue1 else hue1 + 1.0
        val hue2M = if (hueClose || hue2 > hue1) hue2 else hue2 + 1.0
        val hue   = hue1M * w1 + hue2M * w2
        val sat   = sat1  * w1 + sat2  * w2
        val bri   = bri1  * w1 + bri2  * w2
        val rgb   = HsbToRgb(hue, sat, bri)
        (a << 24) | rgb
      }
      _Color.User(argb)
    }
  }

  object Mix extends ProductReader[Mix] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Mix = {
      require (arity == 4 && adj == 0)
      val _a    = in.readEx[_Color]()
      val _b    = in.readEx[_Color]()
      val _w    = in.readEx[Double]()
      val _mode = in.readEx[Int]()
      Mix(_a, _b, _w, _mode)
    }
  }
  /** Mixes two colors.
    *
    * @param w     weight from zero (purely color `a`) to one (purely color `b`).
    * @param mode  0 to mix based on RGB model, 1 to mix based on HSB model.
    */
  final case class Mix(a: Ex[Color], b: Ex[Color], w: Ex[Double] = 0.5, mode: Ex[Int] = 0)
    extends Ex[Color] {

    override def productPrefix: String = s"Color$$Mix" // serialization

    type Repr[T <: Txn[T]] = IExpr[T, Color]

    protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
      import ctx.targets
      new QuaternaryOp.Expanded(MixOp(), a.expand[T], b.expand[T], w.expand[T], mode.expand[T], tx)
    }
  }
}
