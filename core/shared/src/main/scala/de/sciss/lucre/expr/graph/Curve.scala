/*
 *  Curve.scala
 *  (SoundProcesses)
 *
 *  Copyright (c) 2010-2025 Hanns Holger Rutz. All rights reserved.
 *
 *	This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.lucre.expr.graph

import de.sciss.lucre.Adjunct.{FromAny, HasDefault}
import de.sciss.lucre.expr.Context
import de.sciss.lucre.expr.graph.{Const => _Const}
import de.sciss.lucre.expr.ExElem.{ProductReader, RefMapIn}
import de.sciss.lucre.expr.impl.BasicExObjBridgeImpl
import de.sciss.lucre.{Adjunct, IExpr, Txn}
import de.sciss.proc.CurveObj
import de.sciss.proc.ExImport.Curve
import de.sciss.serial.DataInput
import de.sciss.synth.{Curve => _Curve}

import scala.annotation.switch

object Curve {
  private lazy val _init: Unit =
    Adjunct.addFactory(TypeImpl)

  def init(): Unit = _init

  def Step    : Ex[Curve] = Const(_Curve.step       . id)
  def Lin     : Ex[Curve] = Const(_Curve.linear     . id)
  def Exp     : Ex[Curve] = Const(_Curve.exponential. id)
  def Sine    : Ex[Curve] = Const(_Curve.sine       . id)
  def Welch   : Ex[Curve] = Const(_Curve.welch      . id)
  def Squared : Ex[Curve] = Const(_Curve.squared    . id)
  def Cubed   : Ex[Curve] = Const(_Curve.cubed      . id)

  private[sciss] object Const extends ProductReader[Ex[Curve]] {
    def apply(id: Int): Ex[Curve] = Impl(id)

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Ex[Curve] = {
      require (arity == 1 && adj == 0)
      val _id = in.readInt()
      Const(_id)
    }

    private final case class Impl(id: Int) extends Ex[Curve] {
      type Repr[T <: Txn[T]] = IExpr[T, Curve]

      override def productPrefix: String = s"Curve$$Const" // serialization

      override protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
        val peer = (id: @switch) match {
          case _Curve.step       .id  => _Curve.step
          case _Curve.linear     .id  => _Curve.linear
          case _Curve.exponential.id  => _Curve.exponential
          case _Curve.sine       .id  => _Curve.sine
          case _Curve.welch      .id  => _Curve.welch
          case _Curve.squared    .id  => _Curve.squared
          case _Curve.cubed      .id  => _Curve.cubed
          case other                  => sys.error(s"Unexpected curve shape id $other")
        }
        new _Const.Expanded(peer)
      }
    }
  }

  object Par extends ProductReader[Ex[Curve]] {
    def apply(curvature: Ex[Double]): Ex[Curve] = Impl(curvature)

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Ex[Curve] = {
      require (arity == 1 && adj == 0)
      val _curvature = in.readEx[Double]()
      Par(_curvature)
    }

    private final case class Impl(curvature: Ex[Double]) extends Ex[Curve] {
      override def productPrefix: String = s"Curve$$Par" // serialization

      type Repr[T <: Txn[T]] = IExpr[T, Curve]

      protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
        import ctx.targets
        new UnaryOp.Expanded(ParOp(), curvature.expand[T], tx)
      }
    }
  }

  // only used in expansion -- no reader needed
  private[lucre] final case class ParOp()
    extends UnaryOp.Op[Double, Curve] {

    override def productPrefix: String = s"Curve$$ParOp" // serialization

    def apply(a: Double): Curve = _Curve.parametric(a.toFloat)
  }

  def Type: Obj.Bridge[Curve] with Obj.CanMake[Curve] with HasDefault[Curve] with FromAny[Curve] =
    TypeImpl

  private object TypeImpl extends BasicExObjBridgeImpl[_Curve, CurveObj](CurveObj)
    with HasDefault[_Curve] with FromAny[_Curve] with Adjunct.Factory {

    override def toString: String = "Curve"

    import de.sciss.proc.{CurveObj => tpe}

    final val id = 2007

    // WARNING: this must correspond with the serialization of `AbstractExObjBridgeImpl`!
    override def readIdentifiedAdjunct(in: DataInput): Adjunct = {
      val typeId = in.readInt()
      assert (typeId == tpe.typeId)
      this
    }

    override def defaultValue: _Curve = _Curve.lin

    override def fromAny(in: Any): Option[_Curve] = in match {
      case a: _Curve  => Some(a)
      case _          => None
    }
  }
}
