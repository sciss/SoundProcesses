/*
 *  EnvSegment.scala
 *  (SoundProcesses)
 *
 *  Copyright (c) 2010-2025 Hanns Holger Rutz. All rights reserved.
 *
 *	This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.lucre.expr.graph

import de.sciss.lucre.Adjunct.{FromAny, HasDefault}
import de.sciss.lucre.expr.ExElem.{ProductReader, RefMapIn}
import de.sciss.lucre.expr.{Context, graph}
import de.sciss.lucre.expr.impl.BasicExObjBridgeImpl
import de.sciss.lucre.{Adjunct, IExpr, Txn}
import de.sciss.proc.ExImport.EnvSegment
import de.sciss.proc.{EnvSegment => _EnvSegment}
import de.sciss.serial.DataInput
import de.sciss.synth.{Curve => _Curve}

object EnvSegment {
  private lazy val _init: Unit =
    Adjunct.addFactory(TypeImpl)

  def init(): Unit = _init

  /** Creates a segment with a single (mono) segment. */
  def apply(start: Ex[Double] = 0.0, curve: Ex[_Curve] = graph.Curve.Lin): Ex[EnvSegment] =
    Single(start, curve)

  object Single extends ProductReader[Ex[EnvSegment]] {
    def apply(start: Ex[Double], curve: Ex[_Curve] = graph.Curve.Lin): Ex[EnvSegment] =
      new Single(start, curve)

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Ex[EnvSegment] = {
      require (arity == 2 && adj == 0)
      val _start = in.readEx[Double]()
      val _curve = in.readEx[_Curve]()
      Single(_start, _curve)
    }
  }

  private final case class Single(start: Ex[Double], curve: Ex[_Curve])
    extends Ex[EnvSegment] {

    override def productPrefix: String = s"EnvSegment$$Single" // serialization

    type Repr[T <: Txn[T]] = IExpr[T, EnvSegment]

    protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
      import ctx.targets
      new BinaryOp.Expanded(SingleOp(),
        start.expand[T], curve.expand[T], tx)
    }
  }

  // only used in expansion -- no reader needed
  private[lucre] final case class SingleOp()
    extends BinaryOp.Op[Double, _Curve, EnvSegment] {

    override def productPrefix: String = s"EnvSegment$$SingleOp" // serialization

    def apply(a: Double, b: _Curve): EnvSegment =
      _EnvSegment.Single(a, b)
  }

  object Multi extends ProductReader[Ex[EnvSegment]] {
    def apply(start: Ex[Seq[Double]] = Seq(0.0), curve: Ex[_Curve] = graph.Curve.Lin): Ex[EnvSegment] =
      new Multi(start, curve)

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Ex[EnvSegment] = {
      require (arity == 2 && adj == 0)
      val _start = in.readEx[Seq[Double]]()
      val _curve = in.readEx[_Curve ]()
      Multi(_start, _curve)
    }
  }

  private final case class Multi(start: Ex[Seq[Double]], curve: Ex[_Curve])
    extends Ex[EnvSegment] {

    override def productPrefix: String = s"EnvSegment$$Multi" // serialization

    type Repr[T <: Txn[T]] = IExpr[T, EnvSegment]

    protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
      import ctx.targets
      new BinaryOp.Expanded(MultiOp(),
        start.expand[T], curve.expand[T], tx)
    }
  }

  // only used in expansion -- no reader needed
  private[lucre] final case class MultiOp()
    extends BinaryOp.Op[Seq[Double], _Curve, EnvSegment] {

    override def productPrefix: String = s"EnvSegment$$MultiOp" // serialization

    def apply(a: Seq[Double], b: _Curve): EnvSegment =
      _EnvSegment.Multi(a.toIndexedSeq, b)
  }

  object Start extends ProductReader[Start] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Start = {
      require (arity == 1 && adj == 0)
      val _in = in.readEx[_EnvSegment]()
      Start(_in)
    }
  }
  final case class Start(in: Ex[EnvSegment]) extends Ex[Seq[Double]] {
    override def productPrefix: String = s"EnvSegment$$Start" // serialization

    type Repr[T <: Txn[T]] = IExpr[T, Seq[Double]]

    protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
      import ctx.targets
      new UnaryOp.Expanded(StartOp(), in.expand[T], tx)
    }
  }

  // note: this only used in expansion and never serialized
  private final case class StartOp() extends UnaryOp.Op[EnvSegment, Seq[Double]] {
    override def productPrefix: String = s"EnvSegment$$StartOp" // serialization

    override def apply(x: EnvSegment): Seq[Double] = x.startLevels
  }

  object Curve extends ProductReader[Curve] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Curve = {
      require (arity == 1 && adj == 0)
      val _in = in.readEx[_EnvSegment]()
      Curve(_in)
    }
  }
  final case class Curve(in: Ex[_EnvSegment]) extends Ex[_Curve] {
    override def productPrefix: String = s"EnvSegment$$Curve" // serialization

    type Repr[T <: Txn[T]] = IExpr[T, _Curve]

    protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
      import ctx.targets
      new UnaryOp.Expanded(CurveOp(), in.expand[T], tx)
    }
  }

  // note: this only used in expansion and never serialized
  private final case class CurveOp() extends UnaryOp.Op[EnvSegment, _Curve] {
    override def productPrefix: String = s"EnvSegment$$CurveOp" // serialization

    override def apply(x: EnvSegment): _Curve = x.curve
  }

  def Type: Obj.Bridge[EnvSegment] with Obj.CanMake[EnvSegment] with HasDefault[EnvSegment] with FromAny[EnvSegment] =
    TypeImpl

  private object TypeImpl extends BasicExObjBridgeImpl[_EnvSegment, _EnvSegment.Obj](_EnvSegment.Obj)
    with HasDefault[_EnvSegment] with FromAny[_EnvSegment] with Adjunct.Factory {

    override def toString: String = "EnvSegment"

    import _EnvSegment.{Obj => tpe}

    final val id = 2012

    // WARNING: this must correspond with the serialization of `AbstractExObjBridgeImpl`!
    override def readIdentifiedAdjunct(in: DataInput): Adjunct = {
      val typeId = in.readInt()
      assert (typeId == tpe.typeId)
      this
    }

    override def defaultValue: _EnvSegment = _EnvSegment.Single(0, _Curve.linear)

    override def fromAny(in: Any): Option[_EnvSegment] = in match {
      case a: _EnvSegment => Some(a)
      case _              => None
    }
  }
}
