/*
 *  Midi.scala
 *  (SoundProcesses)
 *
 *  Copyright (c) 2010-2025 Hanns Holger Rutz. All rights reserved.
 *
 *	This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.lucre.expr.graph

import de.sciss.lucre.Txn.{peer => txPeer}
import de.sciss.lucre.expr.ExElem.{ProductReader, RefMapIn}
import de.sciss.lucre.expr.impl.IActionImpl
import de.sciss.lucre.expr.{Context, Graph, IAction, IControl, ITrigger, Model}
import de.sciss.lucre.impl.{IChangeEventImpl, IDummyEvent, IEventImpl}
import de.sciss.lucre.{Caching, IChangeEvent, IEvent, IExpr, IPull, ITargets, Txn}

import scala.concurrent.stm.Ref

object Midi extends MidiPlatform {
  object Devices extends ProductReader[Devices] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Devices = {
      require(arity == 0 && adj == 0)
      new Devices
    }
  }
  case class Devices() extends Ex[Seq[Device]] {
    override def productPrefix: String = s"Midi$$Devices" // serialization

    type Repr[T <: Txn[T]] = IExpr[T, Seq[Device]]

    override protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] =
      new ExpandedDevices[T]
  }

  private final class ExpandedDevices[T <: Txn[T]] extends IExpr[T, Seq[Device]] {
    override def value(implicit tx: T): Seq[Device] = listDevices()

    override def changed: IChangeEvent[T, Seq[Device]] = IDummyEvent.change

    override def dispose()(implicit tx: T): Unit = ()
  }

  private final val CONTROL_CHANGE  = 0xB0  // 176
  private final val NOTE_ON         = 0x90  // 144
  private final val NOTE_OFF        = 0x80  // 128
  private final val PITCH_BEND      = 0xE0  // 224

  // generic construction
  object In extends ProductReader[In] /*with MidiInPlatform*/ {
    /** Creates a Midi input from a given device descriptor */
    def apply(dev: Ex[Device]): In = Impl(dev)

    /** Creates a Midi input from a device described by regular expressions.
      * If more than one matching device is found, an arbitrary one is picked.
      * If no matching device is found, a dummy device is used.
      *
      * Note that currently no dynamic device changes are supported, and the matching
      * happens only during the control initialization.
      *
      * @param name   a regular expression for matching the device by name,
      *               or empty to match any device name
      * @param descr  a regular expression for matching the device by description,
      *               or empty to match any device description
      * @param vendor a regular expression for matching the device by vendor,
      *               or empty to match any device vendor
      */
    def apply(name: Ex[String] = "", descr: Ex[String] = "", vendor: Ex[String] = ""): In = {
      val r = Device.RegEx(name, descr, vendor, input = true)
      Impl(r)
    }

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): In = {
      require (arity == 1 && adj == 0)
      val _dev = in.readEx[Device]()
      In(_dev)
    }

    type Repr[T <: Txn[T]] = InRepr[T]

    private final class ReceivedExpanded[T <: Txn[T]](peer: Repr[T], tx0: T)
                                                     (implicit protected val targets: ITargets[T])
      extends ITrigger[T] with IEventImpl[T, Unit] {

      peer.received.--->(changed)(tx0)

      def dispose()(implicit tx: T): Unit =
        peer.received.-/->(changed)

      private[lucre] def pullUpdate(pull: IPull[T])(implicit tx: T): Option[Unit] =
        Trig.Some

      def changed: IEvent[T, Unit] = this
    }

    object Received extends ProductReader[Received] {
      override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Received = {
        require(arity == 1 && adj == 0)
        val _in = in.readProductT[In]()
        new Received(_in)
      }
    }

    final case class Received(in: In) extends Trig {
      type Repr[T <: Txn[T]] = ITrigger[T]

      override def productPrefix = s"Midi$$In$$Received" // serialization

      protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
        val ns = in.expand[T]
        import ctx.targets
        new ReceivedExpanded[T](ns, tx)
      }
    }

    object ShortMessage extends ProductReader[ShortMessage] {
      override def read(in: RefMapIn, key: String, arity: Int, adj: Int): ShortMessage = {
        require(arity == 5 && adj == 0)
        val _in     = in.readProductT[In]()
        val _cmd    = in.readInt()
        val _chan   = in.readProductT[CaseDef[Int]]()
        val _num    = in.readProductT[CaseDef[Int]]()
        val _value  = in.readProductT[CaseDef[Int]]()
        new ShortMessage(_in, _cmd, _chan, _num, _value)
      }
    }

    final case class ShortMessage(in: In, cmd: Int, chan: CaseDef[Int], num: CaseDef[Int], value: CaseDef[Int])
      extends Trig {

      type Repr[T <: Txn[T]] = ITrigger[T]

      override def productPrefix = s"Midi$$In$$ShortMessage" // serialization

      protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
        val inV     = in    .expand[T]
        val chanV   = chan  .expand[T]
        val numV    = num   .expand[T]
        val valueV  = value .expand[T]
        import ctx.targets
        new ShortMessageExpanded[T](inV, cmd, chanV, numV, valueV, tx)
      }
    }

    // The implementation uses `Caching` to make sure it always
    // executes the side-effects. Alternatively, we could have traversed the case-defs and
    // connect the expanded variables?
    private final class ShortMessageExpanded[T <: Txn[T]](in: In.Repr[T],
                                                          cmd: Int,
                                                          chan : CaseDef.Expanded[T, Int],
                                                          data1: CaseDef.Expanded[T, Int],
                                                          data2: CaseDef.Expanded[T, Int],
                                                          tx0: T
                                                         )
                                                         (implicit protected val targets: ITargets[T])
      extends ITrigger[T] with IEventImpl[T, Unit] with Caching {

      // ---- impl ----

      in.received.--->(changed)(tx0)

      override def dispose()(implicit tx: T): Unit = ()

      override private[lucre] def pullUpdate(pull: IPull[T])(implicit tx: T): Option[Unit] = {
        val jMsgOpt: Option[PeerMessage] = pull(in.received)
        val jShortMsgOpt = jMsgOpt.flatMap(matchShortMessage)
        jShortMsgOpt match {
          case Some((`cmd`, chanVal, data1Val, data2Val)) =>
            val matches = {
              var ok = true
              ok = ok && chan .select(chanVal)
              ok = ok && data1.select(data1Val)
              ok = ok && data2.select(data2Val)
              if (ok) {
                chan .commit()
                data1.commit()
                data2.commit()
              }
              ok
            }
            if (matches) Trig.Some else None

          case _ => None
        }
      }

      def changed: IEvent[T, Unit] = this
    }

    object BendMessage extends ProductReader[BendMessage] {
      override def read(in: RefMapIn, key: String, arity: Int, adj: Int): BendMessage = {
        require(arity == 3 && adj == 0)
        val _in     = in.readProductT[In]()
        val _chan   = in.readProductT[CaseDef[Int]]()
        val _data   = in.readProductT[CaseDef[Int]]()
        new BendMessage(_in, _chan, _data)
      }
    }

    final case class BendMessage(in: In, chan: CaseDef[Int], value: CaseDef[Int])
      extends Trig {

      type Repr[T <: Txn[T]] = ITrigger[T]

      override def productPrefix = s"Midi$$In$$BendMessage" // serialization

      protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
        val inV     = in    .expand[T]
        val chanV   = chan  .expand[T]
        val valueV  = value .expand[T]
        import ctx.targets
        new BendMessageExpanded[T](inV, chanV, valueV, tx)
      }
    }

    // The implementation uses `Caching` to make sure it always
    // executes the side-effects. Alternatively, we could have traversed the case-defs and
    // connect the expanded variables?
    private final class BendMessageExpanded[T <: Txn[T]](in: In.Repr[T],
                                                         chan : CaseDef.Expanded[T, Int],
                                                         value: CaseDef.Expanded[T, Int],
                                                         tx0: T
                                                         )
                                                         (implicit protected val targets: ITargets[T])
      extends ITrigger[T] with IEventImpl[T, Unit] with Caching {

      // ---- impl ----

      in.received.--->(changed)(tx0)

      override def dispose()(implicit tx: T): Unit = ()

      override private[lucre] def pullUpdate(pull: IPull[T])(implicit tx: T): Option[Unit] = {
        val jMsgOpt: Option[PeerMessage] = pull(in.received)
        val jShortMsgOpt = jMsgOpt.flatMap(matchShortMessage)
        jShortMsgOpt match {
          case Some((PITCH_BEND, chanVal, data1Val, data2Val)) =>
            val matches = chan.select(chanVal) && {
              val value0 = (data2Val << 7) | data1Val
              // NOTE: JDK bug!! https://www.mail-archive.com/openjdk@lists.launchpad.net/msg12694.html
              val valueV = (value0 + 0x2000) % 0x4000
//              println(s"data1Val $data1Val, data2Val $data2Val, value0 $value0, valueV $valueV")
              value.select(valueV)
            }
            if (matches) {
              chan.commit()
              value.commit()
              Trig.Some
            } else None

          case _ => None
        }
      }

      def changed: IEvent[T, Unit] = this
    }

    private final case class Impl(device: Ex[Device]) extends In { in =>
      type Repr[T <: Txn[T]] = In.Repr[T]

      override protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): in.Repr[T] = {
        val devV = device.expand[T].value
        import ctx.{targets, cursor}
        new InExpanded[T](devV) // .initExpanded()
      }

      override def productPrefix = s"Midi$$In" // serialization

      override def received: Trig = Received(this)

      override def cc(chan: CaseDef[Int], num: CaseDef[Int], value: CaseDef[Int]): Trig =
        ShortMessage(this, CONTROL_CHANGE, chan, num, value)

      override def noteOn(chan: CaseDef[Int], num: CaseDef[Int], velocity: CaseDef[Int]): Trig =
        ShortMessage(this, NOTE_ON, chan, num, velocity)

      override def noteOff(chan: CaseDef[Int], num: CaseDef[Int], velocity: CaseDef[Int]): Trig =
        ShortMessage(this, NOTE_OFF, chan, num, velocity)

      protected def any2stringadd: Any = ()

      override def bend(chan: CaseDef[Int], value: CaseDef[Int]): Trig =
        BendMessage(this, chan, value)

      override def cc14(chan: CaseDef[Int], numHi: CaseDef[Int], numLo: CaseDef[Int], value: Var[Int]): Trig = {
        // XXX TODO we could implement this on a lower level and thus possibly more efficiently
        val valHi = Var[Int](0)
        val valLo = Var[Int](0)
        val mHi   = ShortMessage(this, CONTROL_CHANGE, chan, numHi, valHi)
        val mLo   = ShortMessage(this, CONTROL_CHANGE, chan, numLo, valLo)
        val hasHi = Var[Boolean](false)
        val res   = Trig()
        mHi --> hasHi.set(true)
        mLo.filter(hasHi) --> Act(
          hasHi.set(false),
          value.set((valHi << 7) | valLo),
          res
        )
        res
      }

      //      override def dump: Ex[Int] = ...
      //      override def dump_=(x: Ex[Int]): Unit = ...
    }
  }

  trait In extends Control {
    type Repr[T <: Txn[T]] <: In.Repr[T]

    def received: Trig

    /** Receives controller change messages.
      *
      * All three parameters are `CaseDef` instances, so you could for example filter
      * messages only coming from channel 2 by using `chan = Quote(2)`, or you could
      * receive all channels and find the channel in a variable via `chan = Var(0)`.
      *
      * Note that if you only want to use `value`, you will
      * still have to patch the trigger into an `Act.Nop()` to make sure the events are fired.
      *
      * @param chan  the MIDI channel
      * @param num   the CC number
      * @param value 7-bit value (0 to 127)
      */
    def cc     (chan: CaseDef[Int], num: CaseDef[Int], value    : CaseDef[Int]): Trig
    def noteOn (chan: CaseDef[Int], num: CaseDef[Int], velocity : CaseDef[Int]): Trig
    def noteOff(chan: CaseDef[Int], num: CaseDef[Int], velocity : CaseDef[Int]): Trig

    /** Receives pitch bend messages.
     *
     * All two parameters are `CaseDef` instances, so you could for example filter
     * messages only coming from channel 2 by using `chan = Quote(2)`, or you could
     * receive all channels and find the channel in a variable via `chan = Var(0)`.
     *
     * Note that if you only want to use `value`, you will
     * still have to patch the trigger into an `Act.Nop()` to make sure the events are fired.
     *
     * @param chan    the MIDI channel
     * @param value   14-bit value (0 to 16383)
     */
    def bend(chan: CaseDef[Int], value: CaseDef[Int]): Trig = throw new NotImplementedError()

    /** Receives 14-bit controller change messages with MSB, LSB technique.
      * The device is assumed to send two CC messages, first for the MSB part of the value,
      * second for the LSB part. They are internally combined to create the 14-bit `value`
      * observed by the user.
      *
      * Note that if you only want to use `value`, you will
      * still have to patch the trigger into an `Act.Nop()` to make sure the events are fired.
      *
      * @param chan  the MIDI channel
      * @param numHi the CC number for the MSB part
      * @param numLo the CC number for the LSB part
      * @param value 14-bit value (0 to 16383)
      */
    def cc14(chan: CaseDef[Int], numHi: CaseDef[Int], numLo: CaseDef[Int], value: Var[Int]): Trig

    def device: Ex[Device]

//    def dump: Ex[Int]
//    def dump_=(x: Ex[Int]): Unit
  }

  object Out extends ProductReader[Out] {
    def apply(dev: Ex[Device]): Out = Impl(dev)

    /** Creates a Midi output from a device described by regular expressions.
     * If more than one matching device is found, an arbitrary one is picked.
     * If no matching device is found, a dummy device is used.
     *
     * Note that currently no dynamic device changes are supported, and the matching
     * happens only during the control initialization.
     *
     * @param name   a regular expression for matching the device by name,
     *               or empty to match any device name
     * @param descr  a regular expression for matching the device by description,
     *               or empty to match any device description
     * @param vendor a regular expression for matching the device by vendor,
     *               or empty to match any device vendor
     */
    def apply(name: Ex[String] = "", descr: Ex[String] = "", vendor: Ex[String] = ""): Out = {
      val r = Device.RegEx(name, descr, vendor, input = false)
      Impl(r)
    }

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Out = {
      require(arity == 1 && adj == 0)
      val _dev = in.readEx[Device]()
      Out(_dev)
    }

    type Repr[T <: Txn[T]] = OutRepr[T]

    object ShortMessage extends ProductReader[ShortMessage] {
      override def read(in: RefMapIn, key: String, arity: Int, adj: Int): ShortMessage = {
        require(arity == 5 && adj == 0)
        val _out    = in.readProductT[Out]()
        val _cmd    = in.readInt()
        val _chan   = in.readProductT[Ex[Int]]()
        val _num    = in.readProductT[Ex[Int]]()
        val _value  = in.readProductT[Ex[Int]]()
        new ShortMessage(_out, _cmd, _chan, _num, _value)
      }
    }

    final case class ShortMessage(out: Out, cmd: Int, chan: Ex[Int], num: Ex[Int], value: Ex[Int])
      extends Act {

      type Repr[T <: Txn[T]] = IAction[T]

      override def productPrefix = s"Midi$$Out$$ShortMessage" // serialization

      protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
        val outV    = out   .expand[T]
        val chanV   = chan  .expand[T]
        val numV    = num   .expand[T]
        val valueV  = value .expand[T]
        new ShortMessageExpanded[T](outV, cmd, chanV, numV, valueV)
      }
    }

    private final class ShortMessageExpanded[T <: Txn[T]](out: Out.Repr[T],
                                                          cmd: Int,
                                                          chan  : IExpr[T, Int],
                                                          data1 : IExpr[T, Int],
                                                          data2 : IExpr[T, Int],
                                                         )
      extends IActionImpl[T] {

      def executeAction()(implicit tx: T): Unit = {
        val chanV   = chan  .value
        val data1V  = data1 .value
        val data2V  = data2 .value
        out.send(mkShortMessage(cmd, chanV, data1V, data2V))
      }
    }

    object BendMessage extends ProductReader[BendMessage] {
      override def read(in: RefMapIn, key: String, arity: Int, adj: Int): BendMessage = {
        require(arity == 3 && adj == 0)
        val _out    = in.readProductT[Out]()
        val _chan   = in.readProductT[Ex[Int]]()
        val _value  = in.readProductT[Ex[Int]]()
        new BendMessage(_out, _chan, _value)
      }
    }

    final case class BendMessage(out: Out, chan: Ex[Int], value: Ex[Int])
      extends Act {

      type Repr[T <: Txn[T]] = IAction[T]

      override def productPrefix = s"Midi$$Out$$BendMessage" // serialization

      protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
        val outV    = out   .expand[T]
        val chanV   = chan  .expand[T]
        val valueV  = value .expand[T]
        new BendMessageExpanded[T](outV, chanV, valueV)
      }
    }

    private final class BendMessageExpanded[T <: Txn[T]](out: Out.Repr[T],
                                                         chan  : IExpr[T, Int],
                                                         value : IExpr[T, Int],
                                                         )
      extends IActionImpl[T] {

      def executeAction()(implicit tx: T): Unit = {
        val chanV   = chan  .value
        val valueV  = value .value
        val data1V  = valueV & 0x7F
        val data2V  = (valueV >> 7) & 0x7F
        out.send(mkShortMessage(PITCH_BEND, chanV, data1V, data2V))
      }
    }

    object SysexMessage extends ProductReader[SysexMessage] {
      override def read(in: RefMapIn, key: String, arity: Int, adj: Int): SysexMessage = {
        require(arity == 2 && adj == 0)
        val _out   = in.readProductT[Out]()
        val _data  = in.readProductT[Ex[Seq[Int]]]()
        new SysexMessage(_out, _data)
      }
    }

    final case class SysexMessage(out: Out, data: Ex[Seq[Int]])
      extends Act {

      type Repr[T <: Txn[T]] = IAction[T]

      override def productPrefix = s"Midi$$Out$$SysexMessage" // serialization

      protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
        val outV   = out .expand[T]
        val dataV  = data.expand[T]
        new SysexMessageExpanded[T](outV, dataV)
      }
    }

    private final class SysexMessageExpanded[T <: Txn[T]](out : Out.Repr[T],
                                                          data: IExpr[T, Seq[Int]],
                                                        )
      extends IActionImpl[T] {

      def executeAction()(implicit tx: T): Unit = {
        val dataV = data.value
        val bytes = dataV.iterator.map(_.toByte).toArray // could we be more efficient?
        out.send(mkSysexMessage(bytes))
      }
    }

    private final case class Impl(device: Ex[Device]) extends Out { out =>
      type Repr[T <: Txn[T]] = Out.Repr[T]

      override def productPrefix = s"Midi$$Out" // serialization

      override def cc(chan: Ex[Int], num: Ex[Int], value: Ex[Int]): Act =
        ShortMessage(this, CONTROL_CHANGE, chan, num, value)

      override def noteOn(chan: Ex[Int], num: Ex[Int], velocity: Ex[Int]): Act =
        ShortMessage(this, NOTE_ON, chan, num, velocity)

      override def noteOff(chan: Ex[Int], num: Ex[Int], velocity: Ex[Int]): Act =
        ShortMessage(this, NOTE_OFF, chan, num, velocity)

      override def bend(chan: Ex[Int], value: Ex[Int]): Act =
        BendMessage(this, chan, value)

      override def sysex(data: Ex[Seq[Int]]): Act =
        SysexMessage(this, data)

      override def cc14(chan: Ex[Int], numHi: Ex[Int], numLo: Ex[Int], value: Ex[Int]): Act = {
        // XXX TODO we could implement this on a lower level and thus possibly more efficiently
        val valHi = value >> 7
        val valLo = value & 0x7F
        val mHi   = ShortMessage(this, CONTROL_CHANGE, chan, numHi, valHi)
        val mLo   = ShortMessage(this, CONTROL_CHANGE, chan, numLo, valLo)
        val res   = Trig()
        res --> Act(mHi, mLo)
        res
      }

      override protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): out.Repr[T] = {
        val devV = device.expand[T].value
        new OutExpanded[T](devV) // .initExpanded()
      }
    }
  }

  trait Out extends Control {
    type Repr[T <: Txn[T]] <: Out.Repr[T]

    /** Sends controller change messages.
      *
      * @param chan  the MIDI channel to use
      * @param num   the CC number
      * @param value 7-bit value (0 to 127)
      */
    def cc      (chan: Ex[Int], num: Ex[Int], value   : Ex[Int]): Act
    def noteOn  (chan: Ex[Int], num: Ex[Int], velocity: Ex[Int]): Act
    def noteOff (chan: Ex[Int], num: Ex[Int], velocity: Ex[Int]): Act

    /** Sends pitch bend messages.
     *
     * @param chan    the MIDI channel to use
     * @param value   the 14-bit value (0 to 16383)
     */
    def bend(chan: Ex[Int], value: Ex[Int]): Act = throw new NotImplementedError()

    def sysex(data: Ex[Seq[Int]]): Act = throw new NotImplementedError()

    /** Sends 14-bit controller change messages with MSB, LSB technique.
     * The device is assumed to receive two CC messages, first for the MSB part of the value,
     * second for the LSB part. They user provides the 14-bit `value` directly, which is
     * then internally split up, and will be recombined by the receiving device.
     *
     * @param chan    the MIDI channel to use
     * @param numHi   the CC number for the MSB part
     * @param numLo   the CC number for the LSB part
     * @param value   14-bit value (0 to 16383)
     */
    def cc14(chan: Ex[Int], numHi: Ex[Int], numLo: Ex[Int], value: Ex[Int]): Act

    def device: Ex[Device]
  }

  object Node extends ProductReader[Node] {
    def apply(in: Ex[Device], out: Ex[Device]): Node = {
      val mIn  = Midi.In (in )
      val mOut = Midi.Out(out)
      Impl(mIn, mOut)
    }

    /** Creates a Midi node (combined input and output) from a device described by regular expressions.
      * If more than one matching device is found, an arbitrary one is picked.
      * If no matching device is found, a dummy device is used.
      *
      * Note that currently no dynamic device changes are supported, and the matching
      * happens only during the control initialization.
      *
      * @param name   a regular expression for matching the devices by name,
      *               or empty to match any device name
      * @param descr  a regular expression for matching the devices by description,
      *               or empty to match any device description
      * @param vendor a regular expression for matching the devices by vendor,
      *               or empty to match any device vendor
      */
    def apply(name: Ex[String] = "", descr: Ex[String] = "", vendor: Ex[String] = ""): Node = {
      val rIn   = Device.RegEx(name, descr, vendor, input = true )
      val rOut  = Device.RegEx(name, descr, vendor, input = false)
      val in    = Midi.In (rIn )
      val out   = Midi.Out(rOut)
      Impl(in, out)
    }

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Node = {
      require(arity == 2 && adj == 0)
      val _in  = in.readProductT[In ]()
      val _out = in.readProductT[Out]()
      Impl(_in, _out)
    }

    trait Repr[T <: Txn[T]] extends IControl[T] {
      def in : In .Repr[T]
      def out: Out.Repr[T]
    }

    private final val keyCC = "cc"

    object CC extends ProductReader[CC] {
      override def read(in: RefMapIn, key: String, arity: Int, adj: Int): CC = {
        require (arity == 3 && adj == 0)
        val _node = in.readProductT[Node]()
        val _chan = in.readEx[Int]()
        val _num  = in.readEx[Int]()
        new CC(_node, _chan, _num)
      }
    }
    case class CC(node: Node, chan: Ex[Int], num: Ex[Int]) extends Control with Ex[Int] with Model[Int] { cc =>
      type Repr[T <: Txn[T]] = IControl[T] with IExpr[T, Int]

      override def productPrefix = s"Midi$$Node$$CC" // serialization

      def apply(): Ex[Int] = this

      def update(x: Ex[Int]): Unit = {
        val b = Graph.builder
        b.putProperty(cc, keyCC, x)
      }

      override protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): cc.Repr[T] = {
        import ctx.targets
        val srcOpt    = ctx.getProperty[Ex[Int]](cc, keyCC)
        // println(s"srcOpt $srcOpt")
//        val srcExOpt  = srcOpt.map(_.expand[T])
        val nodeEx    = node.expand[T]
        val chanEx    = chan.expand[T]
        val numEx     = num .expand[T]
        new CCExpanded[T](nodeEx, chanEx, numEx, srcOpt, tx)
      }
    }

    object CC14 extends ProductReader[CC14] {
      override def read(in: RefMapIn, key: String, arity: Int, adj: Int): CC14 = {
        require (arity == 4 && adj == 0)
        val _node   = in.readProductT[Node]()
        val _chan   = in.readEx[Int]()
        val _numHi  = in.readEx[Int]()
        val _numLo  = in.readEx[Int]()
        new CC14(_node, _chan, _numHi, _numLo)
      }
    }
    case class CC14(node: Node, chan: Ex[Int], numHi: Ex[Int], numLo: Ex[Int])
      extends Control with Ex[Int] with Model[Int] { cc =>

      type Repr[T <: Txn[T]] = IControl[T] with IExpr[T, Int]

      override def productPrefix = s"Midi$$Node$$CC14" // serialization

      def apply(): Ex[Int] = this

      def update(x: Ex[Int]): Unit = {
        val b = Graph.builder
        b.putProperty(cc, keyCC, x)
      }

      override protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): cc.Repr[T] = {
        import ctx.targets
        val srcOpt    = ctx.getProperty[Ex[Int]](cc, keyCC)
        // println(s"srcOpt $srcOpt")
        // val srcExOpt  = srcOpt.map(_.expand[T])
        val nodeEx    = node  .expand[T]
        val chanEx    = chan  .expand[T]
        val numHiEx   = numHi .expand[T]
        val numLoEx   = numLo .expand[T]
        new CC14Expanded[T](nodeEx, chanEx, numHiEx, numLoEx, srcOpt, tx)
      }
    }

    // XXX TODO:
    // the `srcOpt` is very tricky and points to an architectural problem in Lucre.
    // if we make a link `<->` between two models, Midi.Node and Slider, for example, normally
    // both would look up each other via `getProperty`, and thus an early call to `expand`
    // would create an infinite loop. To avoid that, we introduce an asymmetry here, expanding
    // `srcOpt` "late", i.e. during `initControl`. Not sure this is totally correct and valid,
    // but seems to work. Now the slider would initially read a '0' value, though. A bit hackish...
    private abstract class CCExpandedBase[T <: Txn[T]](peer: Node.Repr[T], chan: IExpr[T, Int],
                                                       srcOpt: Option[Ex[Int]], tx0: T)
                                                      (implicit ctx: Context[T], protected val targets: ITargets[T])
      extends IControl[T] with IExpr[T, Int] with IChangeEventImpl[T, Int] with Caching {

      // ---- abstract ----

      // return -1 to indicate no new value
      protected def checkMidiIn(numIn: Int, valIn: Int)(implicit tx: T): Int

      protected def sendMidiOut()(implicit tx: T): Unit

      // ---- impl ----

      private[this] val txValue  = Ref(0) // Ref[Int](srcOpt.fold(0)(_.value(tx0)))
//      private[this] val srcExOpt = Ref(Option.empty[IExpr[T, Int]])
      private[this] val srcExOpt = Ref[IExpr[T, Int]](new Const.Expanded[T, Int](0))

      peer.in.received        .--->(changed)(tx0)
//      srcOpt.foreach(_.changed.--->(changed)(tx0))

      override private[lucre] def pullChange(pull: IPull[T])(implicit tx: T, phase: IPull.Phase): Int = {
        val res = if (phase.isBefore) txValue() // pull.resolveExpr(this)
        else {
          val rcvEvt = peer.in.received
          if (pull.contains(rcvEvt)) {
            // println("-- is receiver")
            val jMsgOpt: Option[PeerMessage] = pull(rcvEvt)
            val jShortMsgOpt = jMsgOpt.flatMap(matchShortMessage)
            jShortMsgOpt match {
              case Some((`CONTROL_CHANGE`, chanVal, data1Val, data2Val)) if chanVal == chan.value =>
                val res = checkMidiIn(data1Val, data2Val)
                if (res >= 0) {
                  txValue() = res
                  res
                } else txValue()

              case _ => txValue()
            }
          } else { // source is model source
            // println(s"-- is source; ${srcOpt.isDefined}")
            srcOpt match {
              case Some(src) =>
//                val srcEvt    = src.changed
                val srcEvt    = srcExOpt().changed
                val valNow    = pull.applyChange(srcEvt)
                val valBefore = txValue.swap(valNow)
                if (valNow != valBefore) sendMidiOut()
                valNow

              case None => txValue()
            }
          }
        }
        // println(s"pullChange($phase) -> $res")
        res
      }

//      override def initControl()(implicit tx: T): Unit =
//        if (srcOpt.isDefined) sendMidiOut()

      override def initControl()(implicit tx: T): Unit =
        srcOpt.foreach { src =>
          val srcEx   = src.expand[T]
          srcExOpt()  = srcEx
          txValue()   = srcEx.value
          srcEx.changed.--->(changed)
          sendMidiOut()
        }

      override def value(implicit tx: T): Int = txValue()

      override def changed: IChangeEvent[T, Int] = this

      override def dispose()(implicit tx: T): Unit = {
        peer.in.received        .-/->(changed)
//        srcOpt.foreach(_.changed.-/->(changed))
        srcExOpt().changed.-/->(changed)
      }
    }

    private final class CCExpanded[T <: Txn[T]](peer: Node.Repr[T], chan: IExpr[T, Int], num: IExpr[T, Int],
                                                srcOpt: Option[Ex[Int]], tx0: T)
                                               (implicit ctx: Context[T], targets: ITargets[T])
      extends CCExpandedBase[T](peer, chan, srcOpt, tx0) {

      override protected def checkMidiIn(numIn: Int, valIn: Int)(implicit tx: T): Int =
        if (numIn == num.value) valIn else -1

      override protected def sendMidiOut()(implicit tx: T): Unit = {
        val chanV   = chan  .value
        val data1V  = num   .value
        val data2V  = this  .value
        peer.out.send(mkShortMessage(CONTROL_CHANGE, chanV, data1V, data2V))
      }
    }

    private final class CC14Expanded[T <: Txn[T]](peer: Node.Repr[T], chan: IExpr[T, Int],
                                                  numHi: IExpr[T, Int], numLo: IExpr[T, Int],
                                                  srcOpt: Option[Ex[Int]], tx0: T)
                                               (implicit ctx: Context[T], targets: ITargets[T])
      extends CCExpandedBase[T](peer, chan, srcOpt, tx0) {

      private[this] val txValHi = Ref(0)
      private[this] val txHasHi = Ref(false)

      override protected def checkMidiIn(numIn: Int, valIn: Int)(implicit tx: T): Int =
        if (numIn == numHi.value) {
          txValHi() = valIn
          txHasHi() = true
          -1
        } else if (numIn == numLo.value && txHasHi()) {
          val valHi = txValHi()
          val vv = valHi << 7 | valIn
          txHasHi() = false
          vv
        } else {
          -1
        }

      override protected def sendMidiOut()(implicit tx: T): Unit = {
        val chanV  = chan.value
        val numHiV = numHi.value
        val numLoV = numLo.value
        val vv     = this.value
        val valHi  = vv >> 7
        val valLo  = vv & 0x7F
        val out = peer.out
        out.send(mkShortMessage(CONTROL_CHANGE, chanV, numHiV, valHi))
        out.send(mkShortMessage(CONTROL_CHANGE, chanV, numLoV, valLo))
      }
    }

    private final case class Impl(in: In, out: Out) extends Node { node =>
      type Repr[T <: Txn[T]] = Node.Repr[T]

      override def productPrefix = s"Midi$$Node" // serialization

      override protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): node.Repr[T] = {
        val inEx  = in .expand[T]
        val outEx = out.expand[T]
        new Expanded[T](inEx, outEx)
      }

      override def cc(chan: Ex[Int], num: Ex[Int]): Model[Int] =
        new CC(this, chan, num)

      override def cc14(chan: Ex[Int], numHi: Ex[Int], numLo: Ex[Int]): Model[Int] =
        new CC14(this, chan, numHi, numLo)
    }

    private final class Expanded[T <: Txn[T]](val in: In.Repr[T], val out: Out.Repr[T]) extends Repr[T] {
      override def initControl()(implicit tx: T): Unit = ()
      override def dispose    ()(implicit tx: T): Unit = ()
    }
  }
  trait Node extends Control {
    type Repr[T <: Txn[T]] <: Node.Repr[T]

    /** A model view of controller change input and output.
      * This is useful where the MIDI device allows you to set
      * its internal value state via "feedback", receiving
      * the same CC as it sends.
      */
    def cc(chan: Ex[Int], num: Ex[Int]): Model[Int]

    /** A model view of 14-bit controller change input and output with MSB, LSB technique.
      * The model's value range is 0 to 16383.
      */
    def cc14(chan: Ex[Int], numHi: Ex[Int], numLo: Ex[Int]): Model[Int]

    def in : In
    def out: Out
  }
}