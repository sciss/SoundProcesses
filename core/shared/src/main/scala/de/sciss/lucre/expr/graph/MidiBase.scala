package de.sciss.lucre.expr.graph

import de.sciss.lucre.expr.{Context, IControl}
import de.sciss.lucre.{IEvent, IExpr, Txn}
import de.sciss.lucre.expr.ExElem.{ProductReader, RefMapIn}

import java.util.regex.{Pattern, PatternSyntaxException}
import scala.language.implicitConversions

trait MidiBase {
  type PeerDevice
  type PeerMessage

  trait InRepr[T <: Txn[T]] extends IControl[T] {
    def received: IEvent[T, PeerMessage]
  }

  trait OutRepr[T <: Txn[T]] extends IControl[T] {
    def send(m: PeerMessage)(implicit tx: T): Unit
  }

  protected def listDevices(): Seq[Device]

  object Device {
    object RegEx extends ProductReader[RegEx] {
      override def read(in: RefMapIn, key: String, arity: Int, adj: Int): RegEx = {
        require(arity == 4 && adj == 0)
        val _name   = in.readEx[String]()
        val _descr  = in.readEx[String]()
        val _vendor = in.readEx[String]()
        val _input  = in.readEx[Boolean]()
        new RegEx(_name, _descr, _vendor, _input)
      }
    }
    /** A Midi device defined by regular expression matching. For instance, if you were to look for
      * the Faderfox EC4, you could match the name via `"EC4.*`, or you match the description via
      * `"Faderfox.*"`.
      *
      * @param  name   regular expression for the name, or empty to match any name
      * @param  descr  regular expression for the description, or empty to match any description
      * @param  vendor regular expression for the vendor, or empty to match any vendor
      * @param  input  if `true` matches input devices, otherwise matches output devices
      */
    case class RegEx(name: Ex[String], descr: Ex[String], vendor: Ex[String], input: Ex[Boolean]) extends Ex[Device] {
      override def productPrefix: String = s"Midi$$Device$$RegEx" // serialization

      type Repr[T <: Txn[T]] = IExpr[T, Device]

      override protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
        val devOpt: Option[Device] = try {
          val inputV  = input .expand[T].value
          val outputV = !inputV
          val nameV   = name  .expand[T].value
          // scala.util.matching.Regex#matches not available in Scala 2.12
          val nameR   = if (nameV  .isEmpty) null else Pattern.compile(nameV)   // new Regex(nameV  )
          val descrV  = descr .expand[T].value
          val descrR  = if (descrV .isEmpty) null else Pattern.compile(descrV)  // new Regex(descrV )
          val vendorV = vendor.expand[T].value
          val vendorR = if (vendorV.isEmpty) null else Pattern.compile(vendorV) // new Regex(vendorV)
          val devSeq = listDevices()
          devSeq.find { dev =>
            (dev.isInput == inputV) && (dev.isOutput == outputV) &&
              (nameR   == null || nameR   .matcher(dev.name   ).matches() /*nameR  .matches(dev.name  )*/ ) &&
              (descrR  == null || descrR  .matcher(dev.descr  ).matches() /*descrR .matches(dev.descr )*/ ) &&
              (vendorR == null || vendorR .matcher(dev.vendor ).matches() /*vendorR.matches(dev.vendor)*/ )
          }
        } catch {
          case e: PatternSyntaxException =>
            tx.afterCommit {
              Console.err.println(s"Midi.Device.RegEx: ${e.getMessage}")
            }
            None
        }
        val devV = devOpt.getOrElse(EmptyDevice)
        new Const.Expanded[T, Device](devV)
      }
    }

    object Empty extends ProductReader[Empty] {
      override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Empty = {
        require(arity == 0 && adj == 0)
        new Empty
      }
    }
    case class Empty() extends Ex[Device] {
      override def productPrefix: String = s"Midi$$Device$$Empty" // serialization

      type Repr[T <: Txn[T]] = IExpr[T, Device]

      override protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] =
        new Const.Expanded(EmptyDevice)
    }

    object Name extends ProductReader[Name] {
      override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Name = {
        require(arity == 1 && adj == 0)
        val _in = in.readEx[Device]()
        new Name(_in)
      }
    }
    final case class Name(in: Ex[Device]) extends Ex[String] {
      override def productPrefix: String = s"Midi$$Device$$Name" // serialization

      type Repr[T <: Txn[T]] = IExpr[T, String]

      protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
        import ctx.targets
        new UnaryOp.Expanded(NameOp, in.expand[T], tx)
      }
    }

    // only used in expansion, no serialization needed
    private case object NameOp extends UnaryOp.Op[Device, String] {
      override def apply(in: Device): String = in.name
    }

    object Descr extends ProductReader[Descr] {
      override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Descr = {
        require(arity == 1 && adj == 0)
        val _in = in.readEx[Device]()
        new Descr(_in)
      }
    }

    final case class Descr(in: Ex[Device]) extends Ex[String] {
      override def productPrefix: String = s"Midi$$Device$$Descr" // serialization

      type Repr[T <: Txn[T]] = IExpr[T, String]

      protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
        import ctx.targets
        new UnaryOp.Expanded(DescrOp, in.expand[T], tx)
      }
    }

    // only used in expansion, no serialization needed
    private case object DescrOp extends UnaryOp.Op[Device, String] {
      override def apply(in: Device): String = in.descr
    }

    object Vendor extends ProductReader[Vendor] {
      override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Vendor = {
        require(arity == 1 && adj == 0)
        val _in = in.readEx[Device]()
        new Vendor(_in)
      }
    }

    final case class Vendor(in: Ex[Device]) extends Ex[String] {
      override def productPrefix: String = s"Midi$$Device$$Vendor" // serialization

      type Repr[T <: Txn[T]] = IExpr[T, String]

      protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
        import ctx.targets
        new UnaryOp.Expanded(VendorOp, in.expand[T], tx)
      }
    }

    // only used in expansion, no serialization needed
    private case object VendorOp extends UnaryOp.Op[Device, String] {
      override def apply(in: Device): String = in.vendor
    }

    object IsInput extends ProductReader[IsInput] {
      override def read(in: RefMapIn, key: String, arity: Int, adj: Int): IsInput = {
        require(arity == 1 && adj == 0)
        val _in = in.readEx[Device]()
        new IsInput(_in)
      }
    }

    final case class IsInput(in: Ex[Device]) extends Ex[Boolean] {
      override def productPrefix: String = s"Midi$$Device$$IsInput" // serialization

      type Repr[T <: Txn[T]] = IExpr[T, Boolean]

      protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
        import ctx.targets
        new UnaryOp.Expanded(IsInputOp, in.expand[T], tx)
      }
    }

    // only used in expansion, no serialization needed
    private case object IsInputOp extends UnaryOp.Op[Device, Boolean] {
      override def apply(in: Device): Boolean = in.isInput
    }

    object IsOutput extends ProductReader[IsOutput] {
      override def read(in: RefMapIn, key: String, arity: Int, adj: Int): IsOutput = {
        require(arity == 1 && adj == 0)
        val _in = in.readEx[Device]()
        new IsOutput(_in)
      }
    }

    final case class IsOutput(in: Ex[Device]) extends Ex[Boolean] {
      override def productPrefix: String = s"Midi$$Device$$IsOutput" // serialization

      type Repr[T <: Txn[T]] = IExpr[T, Boolean]

      protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
        import ctx.targets
        new UnaryOp.Expanded(IsOutputOp, in.expand[T], tx)
      }
    }

    // only used in expansion, no serialization needed
    private case object IsOutputOp extends UnaryOp.Op[Device, Boolean] {
      override def apply(in: Device): Boolean = in.isOutput
    }

    implicit def Ops(in: Ex[Device]): Ops = new Ops {
      def name    : Ex[String ] = Name    (in)
      def descr   : Ex[String ] = Descr   (in)
      def vendor  : Ex[String ] = Vendor  (in)
      def isInput : Ex[Boolean] = IsInput (in)
      def isOutput: Ex[Boolean] = IsOutput(in)
    }

    trait Ops {
      def name    : Ex[String ]
      def descr   : Ex[String ]
      def vendor  : Ex[String ]
      def isInput : Ex[Boolean]
      def isOutput: Ex[Boolean]
    }
  }

  trait Device {
    def peer: PeerDevice

    def name    : String
    def descr   : String
    def vendor  : String

    def isInput : Boolean
    def isOutput: Boolean
  }

  private object EmptyDevice extends Device {
    override def peer: PeerDevice = throw new IllegalStateException()

    override def name   : String = ""
    override def descr  : String = ""
    override def vendor : String = ""

    override def isInput  : Boolean = false
    override def isOutput : Boolean = false
  }
}
