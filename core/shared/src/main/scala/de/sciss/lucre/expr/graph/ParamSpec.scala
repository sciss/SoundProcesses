/*
 *  ParamSpec.scala
 *  (SoundProcesses)
 *
 *  Copyright (c) 2010-2025 Hanns Holger Rutz. All rights reserved.
 *
 *	This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.lucre.expr.graph

import de.sciss.lucre.{Adjunct, IExpr, Txn}
import de.sciss.lucre.Adjunct.{FromAny, HasDefault}
import de.sciss.lucre.expr.Context
import de.sciss.lucre.expr.ExElem.{ProductReader, RefMapIn}
import de.sciss.lucre.expr.impl.BasicExObjBridgeImpl
import de.sciss.proc.ExImport.{ParamSpec, Warp}
import de.sciss.proc.{ParamSpec => _ParamSpec}
import de.sciss.serial.DataInput

object ParamSpec extends ProductReader[Ex[ParamSpec]] {
  private lazy val _init: Unit =
    Adjunct.addFactory(TypeImpl)

  def init(): Unit = _init

  def apply(lo: Ex[Double] = 0.0, hi: Ex[Double] = 1.0, warp: Ex[Warp] = Warp.Lin,
            unit: Ex[String] = ""): Ex[ParamSpec] =
    Apply(lo, hi, warp, unit)

//  def Empty(): Ex[ParamSpec] = apply()

  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Ex[ParamSpec] = {
    require (arity == 4 && adj == 0)
    val _lo   = in.readEx[Double]()
    val _hi   = in.readEx[Double]()
    val _warp = in.readEx[Warp  ]()
    val _unit = in.readEx[String]()
    Apply(_lo, _hi, _warp, _unit)
  }

  private final case class Apply(lo: Ex[Double], hi: Ex[Double], warp: Ex[Warp], unit: Ex[String])
    extends Ex[ParamSpec] {

    override def productPrefix: String = "ParamSpec" // serialization

    type Repr[T <: Txn[T]] = IExpr[T, ParamSpec]

    protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
      import ctx.targets
      new QuaternaryOp.Expanded(ApplyOp(),
        lo.expand[T], hi.expand[T], warp.expand[T], unit.expand[T], tx)
    }
  }

  // only used in expansion -- no reader needed
  private[lucre] final case class ApplyOp()
    extends QuaternaryOp.Op[Double, Double, Warp, String, ParamSpec] {

    override def productPrefix: String = s"ParamSpec$$ApplyOp" // serialization

    def apply(a: Double, b: Double, c: Warp, d: String): ParamSpec =
      _ParamSpec(a, b, c, d)
  }

  def Type: Obj.Bridge[ParamSpec] with Obj.CanMake[ParamSpec] with HasDefault[ParamSpec] with FromAny[ParamSpec] =
    TypeImpl

  private object TypeImpl extends BasicExObjBridgeImpl[_ParamSpec, _ParamSpec.Obj](_ParamSpec.Obj)
    with HasDefault[_ParamSpec] with FromAny[_ParamSpec] with Adjunct.Factory {

    override def toString: String = "ParamSpec"

    import _ParamSpec.{Obj => tpe}

    final val id = 2011

    // WARNING: this must correspond with the serialization of `AbstractExObjBridgeImpl`!
    override def readIdentifiedAdjunct(in: DataInput): Adjunct = {
      val typeId = in.readInt()
      assert (typeId == tpe.typeId)
      this
    }

    override def defaultValue: _ParamSpec = _ParamSpec()

    override def fromAny(in: Any): Option[_ParamSpec] = in match {
      case a: _ParamSpec  => Some(a)
      case _          => None
    }
  }
}
