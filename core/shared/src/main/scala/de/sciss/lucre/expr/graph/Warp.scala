/*
 *  Warp.scala
 *  (SoundProcesses)
 *
 *  Copyright (c) 2010-2025 Hanns Holger Rutz. All rights reserved.
 *
 *	This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.lucre.expr.graph

import de.sciss.lucre.{Adjunct, IExpr, Txn}
import de.sciss.lucre.Adjunct.{FromAny, HasDefault}
import de.sciss.lucre.expr.Context
import de.sciss.lucre.expr.graph.{Const => _Const}
import de.sciss.lucre.expr.ExElem.{ProductReader, RefMapIn}
import de.sciss.lucre.expr.impl.BasicExObjBridgeImpl
import de.sciss.proc.ExImport.Warp
import de.sciss.proc.{Warp => _Warp}
import de.sciss.serial.DataInput

import scala.annotation.switch

object Warp {
  private lazy val _init: Unit =
    Adjunct.addFactory(TypeImpl)

  def init(): Unit = _init

  def Lin     : Ex[Warp] = Const(_Warp.Linear     .id)
  def Exp     : Ex[Warp] = Const(_Warp.Exponential.id)
  def Cos     : Ex[Warp] = Const(_Warp.Cosine     .id)
  def Sin     : Ex[Warp] = Const(_Warp.Sine       .id)
  def Fader   : Ex[Warp] = Const(_Warp.Fader      .id)
  def DbFader : Ex[Warp] = Const(_Warp.DbFader    .id)
  def Int     : Ex[Warp] = Const(_Warp.Int        .id)

  private[sciss] object Const extends ProductReader[Ex[Warp]] {
    def apply(id: Int): Ex[Warp] = Impl(id)

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Ex[Warp] = {
      require (arity == 1 && adj == 0)
      val _id = in.readInt()
      Const(_id)
    }

    private final case class Impl(id: Int) extends Ex[Warp] {
      type Repr[T <: Txn[T]] = IExpr[T, Warp]

      override def productPrefix: String = s"Warp$$Const" // serialization

      override protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
        val peer = (id: @switch) match {
          case _Warp.Linear     .id => _Warp.Linear
          case _Warp.Exponential.id => _Warp.Exponential
          case _Warp.Cosine     .id => _Warp.Cosine
          case _Warp.Sine       .id => _Warp.Sine
          case _Warp.Fader      .id => _Warp.Fader
          case _Warp.DbFader    .id => _Warp.DbFader
          case _Warp.Int        .id => _Warp.Int
          case other                => sys.error(s"Unexpected warp shape id $other")
        }
        new _Const.Expanded(peer)
      }
    }
  }

  object Par extends ProductReader[Ex[Warp]] {
    def apply(curvature: Ex[Double]): Ex[Warp] = Impl(curvature)

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Ex[Warp] = {
      require (arity == 1 && adj == 0)
      val _curvature = in.readEx[Double]()
      Par(_curvature)
    }

    private final case class Impl(curvature: Ex[Double]) extends Ex[Warp] {
      override def productPrefix: String = s"Warp$$Par" // serialization

      type Repr[T <: Txn[T]] = IExpr[T, Warp]

      protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
        import ctx.targets
        new UnaryOp.Expanded(ParOp(), curvature.expand[T], tx)
      }
    }
  }

  // only used in expansion -- no reader needed
  private[lucre] final case class ParOp()
    extends UnaryOp.Op[Double, Warp] {

    override def productPrefix: String = s"Warp$$ParOp" // serialization

    def apply(a: Double): Warp = _Warp.Parametric(a)
  }

  def Type: Obj.Bridge[Warp] with Obj.CanMake[Warp] with HasDefault[Warp] with FromAny[Warp] =
    TypeImpl

  private object TypeImpl extends BasicExObjBridgeImpl[_Warp, _Warp.Obj](_Warp.Obj)
    with HasDefault[_Warp] with FromAny[_Warp] with Adjunct.Factory {

    override def toString: String = "Warp"

    import _Warp.{Obj => tpe}

    final val id = 2008

    // WARNING: this must correspond with the serialization of `AbstractExObjBridgeImpl`!
    override def readIdentifiedAdjunct(in: DataInput): Adjunct = {
      val typeId = in.readInt()
      assert (typeId == tpe.typeId)
      this
    }

    override def defaultValue: _Warp = _Warp.Lin

    override def fromAny(in: Any): Option[_Warp] = in match {
      case a: _Warp  => Some(a)
      case _          => None
    }
  }
}
