/*
 *  Color.scala
 *  (SoundProcesses)
 *
 *  Copyright (c) 2010-2025 Hanns Holger Rutz. All rights reserved.
 *
 *	This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.proc

import de.sciss.lucre.Event.Targets
import de.sciss.lucre.expr.ExElem.{ProductReader, RefMapIn}
import de.sciss.lucre.expr.graph.{Ex, Color => _Color}
import de.sciss.lucre.impl.ExprTypeImpl
import de.sciss.lucre.{Event, Ident, Txn, Expr => Epr, Var => LVar}
import de.sciss.serial.{ConstFormat, DataInput, DataOutput}

import scala.collection.immutable.{IndexedSeq => Vec}

object Color {
  final val typeId = 22

  private final val COOKIE = 0x436F // 'Co'

  def init(): Unit = Obj.init()

  implicit object format extends ConstFormat[Color] {
    def write(c: Color, out: DataOutput): Unit = {
      out.writeShort(COOKIE)
      out.writeByte(c.id)
      if (c.id >= 16) out.writeInt(c.argb)
    }

    def read(in: DataInput): Color = {
      val cookie = in.readShort()
      if (cookie != COOKIE) sys.error(s"Unexpected cookie $cookie, expected $COOKIE")
      val id = in.readByte()
      if (id < 16) Palette(id)
      else User(in.readInt())
    }
  }

  object Obj extends ExprTypeImpl[Color, Obj] {
    import Color.{Obj => Repr}

    def typeId: Int = Color.typeId

    final val valueName = "Color"

    override def defaultValue: A = Color.Black

    implicit def valueFormat: ConstFormat[Color] = Color.format

    def tryParse(value: Any): Option[Color] = value match {
      case x: Color => Some(x)
      case _        => None
    }

    override protected def mkConst[T <: Txn[T]](id: Ident[T], value: A)(implicit tx: T): Const[T] =
      new _Const[T](id, value)

    override protected def mkVar[T <: Txn[T]](targets: Targets[T], vr: LVar[T, E[T]], connect: Boolean)
                                             (implicit tx: T): Var[T] = {
      val res = new _Var[T](targets, vr)
      if (connect) res.connect()
      res
    }

    override protected def mkProgram[T <: Txn[T]](targets : Targets[T],
                                                  program : LVar[T, Ex[A]],
                                                  sources : LVar[T, Vec[Event[T, Any]]],
                                                  value   : LVar[T, A],
                                                  connect : Boolean,
                                                 )(implicit tx: T): Program[T] = {
      val res = new _Program[T](targets, programRef = program, sourcesRef = sources, valueRef = value)
      if (connect) res.connect()
      res
    }

    private[this] final class _Const[T <: Txn[T]](val id: Ident[T], val constValue: A)
      extends ConstImpl[T] with Repr[T]

    private[this] final class _Var[T <: Txn[T]](val targets: Targets[T], val ref: LVar[T, E[T]])
      extends VarImpl[T] with Repr[T]

    private[this] final class _Program[T <: Txn[T]](val targets   : Event.Targets[T],
                                                    val programRef: LVar[T, Ex[A]],
                                                    val sourcesRef: LVar[T, Vec[Event[T, Any]]],
                                                    val valueRef  : LVar[T, A]
                                                   )
      extends ProgramImpl[T] with Repr[T]
  }
  sealed trait Obj[T <: Txn[T]] extends Epr[T, Color]

  val Black: Color = Predefined(13, "Black", argb = 0xFF000000)
  val White: Color = Predefined(15, "White", argb = 0xFFFFFFFF)

  /** Palette of sixteen predefined colors. */
  val Palette: Vec[Color] = Vector(
    Predefined( 0, "Dark Blue"  , argb = 0xFF00235C),
    Predefined( 1, "Light Blue" , argb = 0xFF007EFA),
    Predefined( 2, "Cyan"       , argb = 0xFF62F2F5),
    Predefined( 3, "Mint"       , argb = 0xFF34AC71),
    Predefined( 4, "Green"      , argb = 0xFF3CEA3B),
    Predefined( 5, "Yellow"     , argb = 0xFFEEFF00),
    Predefined( 6, "Dark Beige" , argb = 0xFF7D654B),
    Predefined( 7, "Light Beige", argb = 0xFFAA9B72),
    Predefined( 8, "Orange"     , argb = 0xFFFF930D),
    Predefined( 9, "Red"        , argb = 0xFFFF402E),
    Predefined(10, "Maroon"     , argb = 0xFF8D0949),
    Predefined(11, "Fuchsia"    , argb = 0xFFFF06D8),
    Predefined(12, "Purple"     , argb = 0xFFBC00E6),
    Black,
    Predefined(14, "Silver"     , argb = 0xFFA9BBC0),
    White,
  )

  object Predefined extends ProductReader[Predefined] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Predefined = {
      require (arity == 3 && adj == 0)
      val _id   = in.readInt()
      val _name = in.readString()
      val _rgba = in.readInt()
      new Predefined(_id, _name, _rgba)
    }
  }
  final case class Predefined private[Color] (id: Int, name: String, argb: Int) extends Color {
    override def productPrefix: String = s"Color$$Predefined" // serialization
  }

  object User extends ProductReader[User] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): User = {
      require (arity == 1 && adj == 0)
      val _rgba = in.readInt()
      new User(_rgba)
    }
  }
  final case class User(argb: Int) extends Color {
    def name  = "User"
    def id    = 16

    override def productPrefix: String = s"Color$$User" // serialization
  }

  // ---- Ex support ----

  implicit final class ExOps(private val x: Ex[Color]) extends AnyVal {
    /** Raw 24-bit integer color value, with 8-bits for alpha, red, green, blue. */
    def argb      : Ex[Int]     = _Color.ARGB(x)

    /** Alpha component in the range of 0 to 1. Zero is fully transparent, one is fully opaque. */
    def alpha     : Ex[Double]  = _Color.Component(x, 0)
    /** Red component in the range of 0 to 1. */
    def red       : Ex[Double]  = _Color.Component(x, 1)
    /** Green component in the range of 0 to 1. */
    def green     : Ex[Double]  = _Color.Component(x, 2)
    /** Blue component in the range of 0 to 1. */
    def blue      : Ex[Double]  = _Color.Component(x, 3)
    /** Hue component in the HSB model, in the range of 0 to 1. By definition this value is cyclic. */
    def hue       : Ex[Double]  = _Color.Component(x, 4)
    /** Saturation component in the HSB model, in the range of 0 to 1. */
    def saturation: Ex[Double]  = _Color.Component(x, 5)
    /** Brightness component in the HSB model, in the range of 0 to 1. */
    def brightness: Ex[Double]  = _Color.Component(x, 6)
    /** Relative luminance, defined as `0.2126 red + 0.7152 green + 0.0722 blue`. Alpha is not taken into account. */
    def luminance : Ex[Double]  = _Color.Component(x, 7)

    /** Mixes this color with a second color `b`, where the weight (amount) of `b` is `w`
      * and the weight of this color is `1 - b`. This interpolation is done in the RGB model.
      */
    def mix/*RGB*/(b: Ex[Color], w: Ex[Double] = 0.5): Ex[Color] = _Color.Mix(x, b, w /*, mode = 0*/)

    /** Mixes this color with a second color `b`, where the weight of `b` is `w`
      * and the weight of this color is `1 - b`. This interpolation is done in the HSB model.
      */
    def mixHSB(b: Ex[Color], w: Ex[Double] = 0.5): Ex[Color] = _Color.Mix(x, b, w, mode = 1)
  }

  // XXX TODO: remove -- duplicate with TypeImpl now
  implicit object ExValue extends Ex.Value[Color]
}
sealed trait Color extends Product {
  /** Technically the bits are sorted as 'ARGB'! */
  @deprecated("Use argb instead", since = "4.14.1")
  def rgba: Int = argb

  /** Value consisting of the alpha component in bits 24-31, the red component in bits 16-23,
    * the green component in bits 8-15, and the blue component in bits 0-7.
    */
  def argb: Int

  /** The identifier is used for serialization. Predefined
    * colors have an id smaller than 16, user colors have an id of 16.
    */
  def id: Int

  /** Either predefined name or `"User"` */
  def name: String
}
