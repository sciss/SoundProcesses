/*
 *  CurveObj.scala
 *  (SoundProcesses)
 *
 *  Copyright (c) 2010-2025 Hanns Holger Rutz. All rights reserved.
 *
 *	This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.proc

import de.sciss.lucre.Event.Targets
import de.sciss.lucre.expr.graph.Ex
import de.sciss.lucre.impl.ExprTypeImpl
import de.sciss.lucre.{Event, Expr, Ident, Txn, Var => LVar}
import de.sciss.proc
import de.sciss.serial.ConstFormat
import de.sciss.synth.Curve
import de.sciss.synth.UGenSource.Vec

object CurveObj extends ExprTypeImpl[Curve, CurveObj] {
  import proc.{CurveObj => Repr}

  final val typeId = 15
  final val valueFormat: ConstFormat[Curve] = Curve.format

  override def defaultValue: A = Curve.lin

  final val valueName = "Curve"

  def tryParse(value: Any): Option[Curve] = value match {
    case x: Curve => Some(x)
    case _        => None
  }

  override protected def mkConst[T <: Txn[T]](id: Ident[T], value: A)(implicit tx: T): Const[T] =
    new _Const[T](id, value)

  override protected def mkVar[T <: Txn[T]](targets: Targets[T], vr: LVar[T, E[T]], connect: Boolean)
                                           (implicit tx: T): Var[T] = {
    val res = new _Var[T](targets, vr)
    if (connect) res.connect()
    res
  }

  override protected def mkProgram[T <: Txn[T]](targets : Targets[T],
                                                program : LVar[T, Ex[A]],
                                                sources : LVar[T, Vec[Event[T, Any]]],
                                                value   : LVar[T, A],
                                                connect : Boolean,
                                               )(implicit tx: T): Program[T] = {
    val res = new _Program[T](targets, programRef = program, sourcesRef = sources, valueRef = value)
    if (connect) res.connect()
    res
  }

  private final class _Const[T <: Txn[T]](val id: Ident[T], val constValue: A)
    extends ConstImpl[T] with Repr[T]

  private final class _Var[T <: Txn[T]](val targets: Targets[T], val ref: LVar[T, E[T]])
    extends VarImpl[T] with Repr[T]


  private[this] final class _Program[T <: Txn[T]](val targets   : Event.Targets[T],
                                                  val programRef: LVar[T, Ex[A]],
                                                  val sourcesRef: LVar[T, Vec[Event[T, Any]]],
                                                  val valueRef  : LVar[T, A]
                                                 )
    extends ProgramImpl[T] with Repr[T]
}
trait CurveObj[T <: Txn[T]] extends Expr[T, Curve]