/*
 *  ExOps.scala
 *  (SoundProcesses)
 *
 *  Copyright (c) 2010-2025 Hanns Holger Rutz. All rights reserved.
 *
 *	This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.proc

import de.sciss.lucre.Adjunct.{FromAny, HasDefault}
import de.sciss.lucre.expr
import de.sciss.lucre.expr.graph.{Ex, Obj, AudioCue => _AudioCue, Color => _Color, Curve => _Curve, FadeSpec => _FadeSpec, ParamSpec => _ParamSpec, Warp => _Warp, EnvSegment => _EnvSegment}
import de.sciss.lucre.expr.{ExAudioFileSpecOps, ExFileOps}
import de.sciss.proc

import scala.language.implicitConversions

object ExImport extends ExImport
trait ExImport extends expr.ExImport {

  implicit def audioCueType:
         Obj.Bridge  [AudioCue]
    with Obj.CanMake [AudioCue]
    with FromAny     [AudioCue]
    with HasDefault  [AudioCue]
    with FromAny     [AudioCue] = _AudioCue.Type

  implicit def colorType:
         Obj.Bridge  [Color]
    with Obj.CanMake [Color]
    with FromAny     [Color]
    with HasDefault  [Color]
    with FromAny     [Color] = _Color.Type

  implicit def curveType:
         Obj.Bridge  [Curve]
    with Obj.CanMake [Curve]
    with FromAny     [Curve]
    with HasDefault  [Curve]
    with FromAny     [Curve] = _Curve.Type

  implicit def fadeSpecType:
         Obj.Bridge  [FadeSpec]
    with Obj.CanMake [FadeSpec]
    with FromAny     [FadeSpec]
    with HasDefault  [FadeSpec]
    with FromAny     [FadeSpec] = _FadeSpec.Type

  implicit def warpType:
         Obj.Bridge  [Warp]
    with Obj.CanMake [Warp]
    with FromAny     [Warp]
    with HasDefault  [Warp]
    with FromAny     [Warp] = _Warp.Type

  implicit def paramSpecType:
         Obj.Bridge  [ParamSpec]
    with Obj.CanMake [ParamSpec]
    with FromAny     [ParamSpec]
    with HasDefault  [ParamSpec]
    with FromAny     [ParamSpec] = _ParamSpec.Type

  implicit def envSegmentType:
         Obj.Bridge  [EnvSegment]
    with Obj.CanMake [EnvSegment]
    with FromAny     [EnvSegment]
    with HasDefault  [EnvSegment]
    with FromAny     [EnvSegment] = _EnvSegment.Type

  //  implicit val codeExAttrBridge       : Obj.Bridge[Code       ] with Obj.CanMake[Code       ] = new Impl(Code       .Obj)
//  implicit val envSegmentExAttrBridge : Obj.Bridge[EnvSegment ] with Obj.CanMake[EnvSegment ] = new Impl(EnvSegment .Obj)
//  implicit object audioFileSpecIsValue extends Ex.Value[AudioFileSpec]

  type AudioCue       = proc              .AudioCue
  type AudioFileSpec  = de.sciss.audiofile.AudioFileSpec
  type Color          = proc              .Color
  type Curve          = de.sciss.synth    .Curve
  type EnvSegment     = proc              .EnvSegment
  type FadeSpec       = proc              .FadeSpec
  type Warp           = proc              .Warp
  type ParamSpec      = proc              .ParamSpec

  /** The general sample-rate used in objects such as `Timeline`, `Grapheme`, `Transport`, `Scheduler`. */
  final val SampleRate = TimeRef.SampleRate

  implicit def fileOps          (x: Ex[URI          ]): ExFileOps           = new ExFileOps(x)
  implicit def audioFileSpecOps (x: Ex[AudioFileSpec]): ExAudioFileSpecOps  = new ExAudioFileSpecOps(x)
}

