/*
 *  AsyncResource.scala
 *  (SoundProcesses)
 *
 *  Copyright (c) 2010-2025 Hanns Holger Rutz. All rights reserved.
 *
 *	This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.proc.impl

import de.sciss.lucre.{Disposable, Txn}
import de.sciss.proc.AuralNode
import de.sciss.processor.impl.FutureWrapper

import scala.concurrent.{ExecutionContext, Future}

object AsyncResource {
  /** Wraps code that directly operates on an `AuralNode` into an `AsyncResource`.
    * That resource's future/processor is already completed.
    */
  def wrapSync[T <: Txn[T]](prepareFun: T => AuralNode[T] => Unit)(disposeFun: T => Unit = (_: T) => ())
                           (implicit exec: ExecutionContext): AsyncResource[T] =
    new WrapSync[T](prepareFun, disposeFun)

  private final class WrapSync[T <: Txn[T]](prepareFun: T => AuralNode[T] => Unit, disposeFun: T => Unit)
                                           (implicit exec: ExecutionContext)
    extends FutureWrapper("wrapSync", Future.unit) with AsyncResource[T] {

    override def install(b: AuralNode[T])(implicit tx: T): Unit =
      prepareFun(tx)(b)

    override def dispose()(implicit tx: T): Unit =
      disposeFun(tx)
  }
}
trait AsyncResource[T <: Txn[T]] extends Future[Any] with Disposable[T] {
  def install(b: AuralNode[T] /* NodeRef.Full[T] */)(implicit tx: T): Unit
}