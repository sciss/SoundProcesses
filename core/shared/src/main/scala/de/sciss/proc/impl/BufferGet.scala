/*
 *  BufferGet.scala
 *  (SoundProcesses)
 *
 *  Copyright (c) 2010-2025 Hanns Holger Rutz. All rights reserved.
 *
 *	This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.proc.impl

import de.sciss.lucre.expr.graph.{Const => ExConst, Var => ExVar}
import de.sciss.lucre.synth.{Buffer, Executor, RT, Resource, Synth}
import de.sciss.lucre.{Expr, Source, Txn, synth}
import de.sciss.lucre.{Var => LVar}
import de.sciss.osc
import de.sciss.proc.{AuralContext, AuralNode, SoundProcesses}
import de.sciss.synth.message.BufferInfo
import de.sciss.synth.proc.graph.Action
import de.sciss.synth.proc.graph.impl.SendReplyResponder
import de.sciss.synth.{GE, message, ugen}

import scala.collection.immutable.{IndexedSeq => Vec}
import scala.collection.{IndexedSeq => CVec}
import scala.concurrent.Future

/** Asynchronously transfers a buffer's content to an `IExpr` chunks by chunk.
  * This works in two steps, as it is triggered from within the node.
  * First, the `Starter` is installed on the aural node. When it detects
  * the trigger, it calls `BufferGet.apply` (which is similar to `BufferPrepare()`).
  */
object BufferGet {
  // via SendReply
  def replyName(key: String): String = s"/$$gb_$key"

  // via n_set
  def doneName (key: String): String = s"/$$gb_done_$key"

  def makeUGen(g: Action.GetBuf): GE = {
    import g._
    import ugen._
    val bufFrames     = BufFrames     .ir(buf)
    val bufChannels   = BufChannels   .ir(buf)
    val values: GE = Seq(
      buf           ,
      bufFrames     ,
      bufChannels   ,
      numFrames     ,
      startFrame    ,
    )
    SendReply.kr(trig = trig, values = values, msgName = BufferGet.replyName(key), id = 0)
    ControlProxyFactory.fromString(BufferGet.doneName(key)).tr
  }

  final class WithEx[T <: Txn[T], A](vr: ExVar.Expanded[T, A])
    extends Sink[T] {

    override def trySet(value: Vec[Double])(implicit tx: T): Unit = trySetAny(value)

    private def trySetAny(value: Any)(implicit tx: T): Unit =
      vr.fromAny.fromAny(value).foreach { valueT =>
        vr.update(new ExConst.Expanded[T, A](valueT))
      }
  }
//  final class WithExpr[T <: Txn[T], A, Repr[~ <: Txn[~]] <: Expr[~, A]](vrH: Source[T, LVar[T, Repr[T]]],
//                                                                         key: String, synth: Synth)
//                                                                        (lift: Vec[Double] => A)
//                                                                        (implicit context: AuralContext[T],
//                                                                         tpe: Expr.Type[A, Repr])
//    extends Sink[T] {
//
//    override def trySet(value: Vec[Double])(implicit tx: T): Unit = {
//      val c   = tpe.newConst[T](lift(value))
//      val vr  = vrH()
//      vr.update(c)
//    }
//  }

  final class WithVec[T <: Txn[T], A, Repr[~ <: Txn[~]] <: Expr[~, Vec[A]]](vrH: Source[T, LVar[T, Repr[T]]])
                                                                            (lift: Vec[Double] => Vec[A])
                                                                            (implicit context: AuralContext[T], tpe: Expr.Type[Vec[A], Repr])
    extends Sink[T] {

    override def trySet(value: Vec[Double])(implicit tx: T): Unit = {
      val c   = tpe.newConst[T](lift(value))
      val vr  = vrH()
      vr.update(c)
    }
  }

  sealed trait Sink[T <: Txn[T]] {
    def trySet(value: Vec[Double])(implicit tx: T): Unit
  }

  final class Starter[T <: synth.Txn[T]](sink: Sink[T], key: String, nr: AuralNode[T])
                                        (implicit context: AuralContext[T])
    extends SendReplyResponder {

    private[this] val Name    = replyName(key)
    private[this] val NodeId  = synth.peer.id

    override protected def synth: Synth = nr.synth

    override protected def added()(implicit tx: RT): Unit = ()

    override protected val body: Body = {
      case osc.Message(Name, NodeId, 0,
        bufIdF          : Float,
        bufFramesF      : Float,
        bufChannelsF    : Float,
        numFramesF      : Float,
        startFrameF     : Float,
      ) =>

        val bufId           = bufIdF          .toInt
        val bufFrames       = bufFramesF      .toInt
        val bufChannels     = bufChannelsF    .toInt
        val startFrame      = startFrameF     .toInt
        val numFrames0      = numFramesF      .toInt
        val numFrames       = if (numFrames0 >= 0) numFrames0 else bufFrames - startFrame
        val server          = nr.server
        val sPeer           = server.peer
        val bufInfo         = BufferInfo.Data(bufId = bufId, numFrames = bufFrames, numChannels = bufChannels,
          sampleRate = server.sampleRate.toFloat /*bufSampleRate*/) // N.B. sample-rate is not used
        val bufPeer         = bufInfo.asBuffer(sPeer)

        import context.universe.cursor
        SoundProcesses.step[T](s"BufferGet($synth, $key)") { implicit tx: T =>
          val buf     = Buffer.wrap(server, bufPeer)
//          val config  = Config(numFrames = numFrames, offset = startFrame, buf = buf, key = key)
          val bw      = BufferGet[T](sink, numFrames = numFrames, offset = startFrame, buf = buf, key = key)
          nr.addResource(bw)
          tx.afterCommit {
            bw.foreach { _ =>
              val server  = nr.server
              val sPeer   = server.peer
              sPeer ! message.NodeSet(synth.peer.id, BufferGet.doneName(key) -> 1f)
            } (Executor.executionContext)
          }
        }
    }
  }

//  /** The configuration of the buffer transfer.
//    *
//    * @param numFrames  the number of frames in the buffer to obtain
//    * @param offset     the offset into the buffer to start from
//    * @param buf        the buffer to write from.
//    * @param key        the key of the `graph.Buffer` element, used for setting the synth control eventually
//    */
//  case class Config(numFrames: Int, offset: Int, buf: Buffer, key: String) {
//    override def productPrefix = "BufferGet.Config"
//    override def toString: String =
//      s"$productPrefix(numFrames = $numFrames, offset = $offset, key = $key)"
//  }

  /** Creates and launches the process.
    *
    * @param numFrames  the number of frames in the buffer to obtain
    * @param offset     the offset into the buffer to start from
    * @param buf        the buffer to write from.
    * @param key        the key of the `graph.Buffer` element, used for setting the synth control eventually
    */
  def apply[T <: synth.Txn[T]](sink: Sink[T], numFrames: Int, offset: Int, buf: Buffer, key: String)
                              (implicit tx: T, context: AuralContext[T]): Future[Any] with Resource = {
    if (!buf.isOnline) sys.error("Buffer must be allocated")
    if (numFrames > buf.numFrames) sys.error(s"Client buffer ($numFrames frames) is larger than buffer (${buf.numFrames})")
    val blockSize = BufferTransfer.calcBlockSize(buf, numChannels = 1 /*buf.numChannels*/)
    val res = new GetN[T](sink, numFramesB = numFrames, off0B = offset, blockSize = blockSize, buf = buf, key = key)
    tx.afterCommit(res.start()(Executor.executionContext))
    res
  }

  // numFramesB: regarding buffer's numChannels, not flat as in the resulting Vec[Double]
  private final class GetN[T <: synth.Txn[T]](sink: Sink[T], numFramesB: Int, off0B: Int, blockSize: Int, buf: Buffer,
                                              key: String)(implicit context: AuralContext[T])
    extends BufferTransfer.GetN[T](
      blockSize   = blockSize,
      numFrames   = numFramesB  * buf.numChannels,
      offset0     = off0B       * buf.numChannels,
      buf         = buf,
      key         = key
    ) {

    // Note: we always produce the interleaved version at the client side
    override protected val numChannels: Int = 1 // buf.numChannels

    private[this] val builder = {
      val b = Vec.newBuilder[Double]
      b.sizeHint(numFramesB * buf.numChannels)
      b
    }

    override protected def runBody(): Future[Prod] = {
//      val afBuf = allocBuf()
      val fut0 = runCore { dataSq =>
        val itSq = dataSq.iterator
        while (itSq.hasNext) {
          val data: CVec[Float] = itSq.next()
          val it: Iterator[Float] = data.iterator
          while (it.hasNext) {
            builder += it.next().toDouble
          }
        }
        Future.unit
      }

      fut0.andThen { case _ =>
        val vec = builder.result()
        import context.universe.cursor
        SoundProcesses.step[T](s"BufferGet($key).done") { implicit tx: T =>
          sink.trySet(vec)
        }
      }
    }

    override def toString = s"BufferGet.GetN($buf)@${hashCode().toHexString}"
  }
}
