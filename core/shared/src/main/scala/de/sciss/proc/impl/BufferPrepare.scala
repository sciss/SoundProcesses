/*
 *  BufferPrepare.scala
 *  (SoundProcesses)
 *
 *  Copyright (c) 2010-2025 Hanns Holger Rutz. All rights reserved.
 *
 *	This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.proc.impl

import de.sciss.audiofile.{AudioFile, AudioFileSpec}
import de.sciss.lucre.synth.{Buffer, Executor, RT}
import de.sciss.lucre.{Artifact, Txn, synth}
import de.sciss.osc
import de.sciss.proc.AuralNode
import de.sciss.processor.impl.{FutureWrapper, ProcessorBase}
import de.sciss.synth.message.BufferGen
import de.sciss.synth.proc.graph

import java.util.concurrent.TimeUnit
import scala.concurrent.stm.Ref
import scala.concurrent.stm.TxnExecutor.{defaultAtomic => atomic}
import scala.concurrent.{Future, TimeoutException}
import scala.math.min

/** Asynchronously prepares a buffer's content by reading successive chunks from a file. */
object BufferPrepare {

  /** The configuration of the buffer preparation.
    *
    * @param f        the audio file to read in
    * @param spec     the file's specification (number of channels and frames)
    * @param offset   the offset into the file to start with
    * @param buf      the buffer to read into. This buffer must have been allocated already.
    * @param key      the key of the `graph.Buffer` element, used for setting the synth control eventually
    */
  case class Config(f: Artifact.Value, spec: AudioFileSpec, offset: Long, buf: Buffer.Modifiable, key: String) {
    override def productPrefix = "BufferPrepare.Config"
    override def toString: String = {
      import spec.{productPrefix => _, _}
      s"$productPrefix($f, numChannels = $numChannels, numFrames = $numFrames, offset = $offset, key = $key)"
    }
  }

  /** Creates and launches the process. */
  def apply[T <: synth.Txn[T]](config: Config)(implicit tx: T): BufferPrepare[T] = {
    import config._
    if (!buf.isOnline) sys.error("Buffer must be allocated")
    val numFrL = spec.numFrames
    if (numFrL > buf.numFrames) sys.error(s"File $f spec ($numFrL frames) is larger than buffer (${buf.numFrames})")
//    if (numFrL > 0x3FFFFFFF) sys.error(s"File $f is too large ($numFrL frames) for an in-memory buffer")
    val isDesktop = f.getScheme == "file" // in the future, we could switch also based on remote/local server
    val res = if (isDesktop) {
      new Read[T](config, numFrames = numFrL.toInt)
    } else {
      val largeBuf  = Executor.isJS || buf.server.config.transport == osc.TCP
      val blockSize = (if (largeBuf) SAMPLES_PER_PACKET_TCP else SAMPLES_PER_PACKET_UDP) / spec.numChannels
      new SetN[T](config, blockSize = blockSize, numFrames = numFrL.toInt)
    }
    tx.afterCommit(res.start()(Executor.executionContext))
    res
  }

  /** Post-processes a buffer preparation with a transformation for partial convolution */
  def partConv[T <: synth.Txn[T]](impulse: BufferPrepare[T], fftSize: Int, partBuf: Buffer.Modifiable)
                                 /*(implicit tx: T)*/: AsyncResource[T] = {
    val res = new PartConv[T](impulse, fftSize = fftSize, partBuf = partBuf)
    // tx.afterCommit(res.start()(Executor.executionContext))
    res
  }

  private final class PartConv[T <: synth.Txn[T]](impulse: BufferPrepare[T], fftSize: Int, partBuf: Buffer.Modifiable)
    extends FutureWrapper[Any]("BufferPrepare.PartConv", impulse)(Executor.executionContext)
      with AsyncResource[T] /*with ProcessorBase[Unit, AsyncResource[T]]*/ {

    override def toString: String = s"BufferPrepare.PartConv($impulse, $fftSize, $partBuf)"

    override def install(b: AuralNode[T])(implicit tx: T): Unit = {
      // println(s"$this install($b)")
      val impulseBuf  = impulse.config.buf
      val genCmd      = BufferGen.PreparePartConv(bufIn = impulseBuf.id, fftSize = fftSize)
      // partBuf.gen(genCmd)
      // explicit, because we want to add dependencies
      tx.addMessage(partBuf, partBuf.peer.genMsg(genCmd), dependencies = impulseBuf :: Nil)
      // XXX TODO: unfortunately, the architecture is not
      // prepared for freeing another `AsyncResource` at this point.
      // So for now, we keep the unused impulse response buffer around
      // until the aural node stops...
      // impulseBuf.dispose()  // no longer needed
      val key     = impulse.config.key
      val ctlName = graph.PartConv.controlName(key)
      b.addControl(ctlName -> partBuf.id)
      val late = Buffer.disposeWithNode(partBuf, b) // XXX TODO -- why was this?
      b.addResource(late)
    }

    override def dispose()(implicit tx: T): Unit = {
      // println(s"$this dispose()")
//      tx.afterCommit(abort())
      if (partBuf.isOnline) partBuf.dispose()
    }
  }

  // use b_read commands (desktop)
  private final class Read[T <: synth.Txn[T]](val config: Config, numFrames: Int)
    extends Impl[T, Double] {

    protected val blockSize: Int = 262144 / config.spec.numChannels

    private[this] val path: String = config.f.getPath

    protected def runBody(): Future[Prod] = {
      def loop(): Future[Unit] = {
        // make sure we always have a `Future` and avoid throwing an exception from `body` directly
        Future.unit.flatMap { _ =>
          val prOpt = nextChunk()
          if (prOpt.isEmpty) abort()
          checkAborted()

          val pr  = prOpt.get
          val fut = config.buf.server.!!(osc.Bundle.now()) // aka 'sync', so we let other stuff be processed first
          awaitFut(fut, pr)(loop())
        }
      }

      loop()
    }

    override protected def prepareNext(offset: Int, chunk: Int, endProgress: Double)(implicit tx: RT): Double = {
      config.buf.read(path, fileStartFrame = offset + config.offset, numFrames = chunk, bufStartFrame = offset)
      endProgress
    }

    override def toString = s"BufferPrepare.Read($path, ${config.buf})@${hashCode().toHexString}"
  }

  private final val SAMPLES_PER_PACKET_TCP = 6540 // ensure < 32K OSC bundle size<
  private final val SAMPLES_PER_PACKET_UDP = 1608 // ensure <  8K OSC bundle size<

  private final class NextData(/*val fileStartFrame: Long,*/ val numFrames: Int, val bufStartFrame: Int,
                               val endProgress: Double)

  // use b_setn commands (browser)
  private final class SetN[T <: synth.Txn[T]](val config: Config, protected val blockSize: Int, numFrames: Int)
    extends Impl[T, NextData] {

    private final val DEBUG = false

    private[this] val numChannels = config.spec.numChannels

    protected def runBody(): Future[Prod] = {
      var SPENT_READING = 0L
      var SPENT_SENDING = 0L
      AudioFile.openReadAsync(config.f).flatMap { af =>
        if (config.offset > 0) af.seek(config.offset)
        val afBuf = af.buffer(blockSize)

        def loop(): Future[Unit] = {
          val nextOpt = nextChunk()
          if (nextOpt.isEmpty) abort()
          checkAborted()

          val next  = nextOpt.get
          val chunk = next.numFrames
          val T_READ1 = if (DEBUG) System.currentTimeMillis() else 0L
          var T_SEND1 = 0L
          val futRead = af.read(afBuf, off = 0, len = chunk)
          val fut = futRead.map { _ =>
            if (DEBUG) {
              val T_READ2 = System.currentTimeMillis()
              SPENT_READING += (T_READ2 - T_READ1)
            }
            val data = Vector.newBuilder[Float]
            data.sizeHint(chunk * numChannels)
            var i = 0
            while (i < chunk) {
              var ch = 0
              while (ch < numChannels) {
                val chBuf = afBuf(ch)
                data += chBuf(i).toFloat // interleave channel data
                ch += 1
              }
              i += 1
            }
            checkAborted()
            val smpOff = next.bufStartFrame * numChannels
            if (DEBUG) T_SEND1 = System.currentTimeMillis()
//            buf.server.!!(osc.Bundle.now(buf.peer.setnMsg((smpOff, data.result()))))
            val buf = config.buf
            buf.server.!(buf.peer.setnMsg((smpOff, data.result())))
          }

          awaitFut(fut, next.endProgress) {
            if (DEBUG) {
              val T_SEND2 = System.currentTimeMillis()
              SPENT_SENDING += (T_SEND2 - T_SEND1)
            }
            loop()
          }
        }

        loop().andThen { case x =>
          if (DEBUG) println(s"BUF PREPARE - time spent reading $SPENT_READING, spent sending $SPENT_SENDING; $x")
          af.close()
        }
      }
    }

    override protected def prepareNext(offset: Int, chunk: Int, endProgress: Double)(implicit tx: RT): NextData =
      new NextData(/*fileStartFrame = offset + off0,*/ numFrames = chunk, bufStartFrame = offset,
        endProgress = endProgress)

    override def toString = s"BufferPrepare.SetN(${config.f}, ${config.buf})@${hashCode().toHexString}"
  }

  private abstract class Impl[T <: synth.Txn[T], Next]
    extends BufferPrepare[T] with ProcessorBase[Unit, AsyncResource[T]] { self =>

    // ---- abstract ----

    protected def prepareNext(offset: Int, chunk: Int, endProgress: Double)(implicit tx: RT): Next

    protected def blockSize: Int

    // ---- impl ----

    final type Prod = Unit

    private val offsetRef = Ref(0)

    protected val numFrames: Long = config.spec.numFrames

    // ---- processor body ----

    protected final def awaitFut(fut: Future[Any], pr: Double)(loop: => Future[Unit]): Future[Unit] =
      if (fut.isCompleted) {
        progress = pr
        if (progress == 1.0) Future.unit else loop

      } else {
        // check once a second if processor was aborted
        val futTimeOut = Executor.timeOut(fut, 1L, TimeUnit.SECONDS).recover {
          case _: TimeoutException => ()
        }
        futTimeOut.flatMap { _ =>
          checkAborted()
          awaitFut(fut, pr)(loop)
        }
      }

    protected def nextChunk(): Option[Next] =
      atomic { implicit tx =>
        val offset  = offsetRef()
        val chunk   = min(numFrames - offset, blockSize).toInt
        val stop    = offset + chunk
        if (chunk > 0) {
          offsetRef() = stop
          implicit val ptx: RT = RT.wrap(tx)
          // buf might be offline if dispose was called
          if (config.buf.isOnline) {
            val pr  = if (stop == numFrames) 1.0 else min(0.9999, stop.toDouble / numFrames)
            val res = prepareNext(offset = offset, chunk = chunk, endProgress = pr)
            Some(res)
          } else None
        } else None
      }

    // ----

    private[this] val installed = Ref(false)

    final def install(b: AuralNode[T] /* NodeRef.Full[T] */)(implicit tx: T): Unit = {
      import Txn.peer
      require(!installed.swap(true))
      val ctlName = graph.Buffer.controlName(config.key)
      val buf = config.buf
      b.addControl(ctlName -> buf.id)
      val late = Buffer.disposeWithNode(buf, b)
      b.addResource(late)
    }

    def dispose()(implicit tx: T): Unit = {
      import Txn.peer
      tx.afterCommit(abort())
      val buf = config.buf
      if (buf.isOnline && !installed()) buf.dispose()
    }
  }
}
trait BufferPrepare[T <: Txn[T]] extends AsyncResource[T] {
  def config: BufferPrepare.Config
}