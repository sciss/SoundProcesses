/*
 *  BufferTransfer.scala
 *  (SoundProcesses)
 *
 *  Copyright (c) 2010-2025 Hanns Holger Rutz. All rights reserved.
 *
 *	This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.proc.impl

import de.sciss.lucre.synth
import de.sciss.lucre.synth.{Buffer, Executor, RT, Resource}
import de.sciss.osc
import de.sciss.processor.impl.ProcessorBase
import de.sciss.synth.message

import java.util.concurrent.TimeUnit
import scala.collection.{IndexedSeq => CVec}
import scala.concurrent.stm.Ref
import scala.concurrent.stm.TxnExecutor.{defaultAtomic => atomic}
import scala.concurrent.{Future, TimeoutException}
import scala.math.min

/** Asynchronously reads a buffer's content, chunks by chunk.
  * This works in two steps, as it is triggered from within the node.
  */
object BufferTransfer {
  private final val SAMPLES_PER_PACKET_TCP = 6540 // ensure < 32K OSC bundle size
  private final val SAMPLES_PER_PACKET_UDP = 1608 // ensure <  8K OSC bundle size

  /** Creates and launches the process. */
  def calcBlockSize(buf: Buffer, numChannels: Int)/*(implicit tx: RT)*/: Int = {
    // if (!buf.isOnline) sys.error("Buffer must be allocated")
    val largeBuf  = Executor.isJS || buf.server.config.transport == osc.TCP
    val blockSize = (if (largeBuf) SAMPLES_PER_PACKET_TCP else SAMPLES_PER_PACKET_UDP) / numChannels
    blockSize
  }

  final val MAX_CLUMP = 10

  private[impl] final class NextData(val numFrames: Int, val bufStartFrame: Int, val endProgress: Double)

  // use b_getn commands
  private[impl] abstract class GetN[T <: synth.Txn[T]](numFrames: Int, offset0: Int,
                                                       blockSize: Int, buf: Buffer, key: String)
    extends Impl[T, NextData](
      blockSize   = blockSize,
      numFrames   = numFrames,
      offset0     = offset0,
      buf         = buf,
      key         = key
    ) {

    // --- abstract ---

    protected def numChannels: Int

//    protected def nextBuf(buf: Array[Array[Double]], off: Int, len: Int): Future[Unit]

    // --- impl ---

//    protected final def allocBuf(): Array[Array[Double]] =
//      Array.ofDim[Double](numChannels, blockSize * MAX_CLUMP)

    private final val DEBUG = false

    private[this] var SPENT_WRITING   = 0L
    private[this] var SPENT_RECEIVING = 0L

    protected final def runCore(consume: Seq[CVec[Float]] => Future[Unit]): Future[Unit] = {
      // val afBuf = Array.ofDim[Double](numChannels, blockSize * MAX_CLUMP)

      def loop(): Future[Unit] = {
        val nextOptSq = Seq.fill(MAX_CLUMP)(nextChunk())
        if (nextOptSq.forall(_.isEmpty)) abort()
        checkAborted()

        val nextSq        = nextOptSq.flatten
        val T_RECEIVE1  = if (DEBUG) System.currentTimeMillis() else 0L
        val futSendSq: Seq[Future[CVec[Float]]] = nextSq.map { next =>
          val smpOff      = next.bufStartFrame * numChannels
          val chunk       = next.numFrames
          val smpChunk    = chunk * numChannels
          val b           = buf.peer
          b.server.!!(b.getnMsg(smpOff until (smpOff + smpChunk))) {
            case message.BufferSetn(b.id, (`smpOff`, xs)) => xs
          }
        }
        val futSend: Future[Seq[CVec[Float]]] = Future.sequence(futSendSq)

        var T_WRITE1 = 0L
        val fut = futSend.flatMap { dataSq =>
          if (DEBUG) {
            val T_RECEIVE2 = System.currentTimeMillis()
            SPENT_RECEIVING += (T_RECEIVE2 - T_RECEIVE1)
          }
          checkAborted()
          if (DEBUG) T_WRITE1 = System.currentTimeMillis()
          // af.write(afBuf, off = 0, len = chunk)
          // nextBuf(afBuf, off = 0, len = chunk)
          consume(dataSq)
        }

        val endProgress = nextSq.last.endProgress
        awaitFut(fut, endProgress) {
          if (DEBUG) {
            val T_WRITE2 = System.currentTimeMillis()
            SPENT_WRITING += (T_WRITE2 - T_WRITE1)
          }
          loop()
        }
      }

      val fut0 = loop()
      if (!DEBUG) fut0 else fut0.andThen { case x =>
        println(s"BUF WRITE - time spent writing $SPENT_WRITING, spent receiving $SPENT_RECEIVING; $x")
      }
    }

    override protected def prepareNext(offset: Int, chunk: Int, endProgress: Double)(implicit tx: RT): NextData =
      new NextData(/*fileStartFrame = offset + off0,*/ numFrames = chunk, bufStartFrame = offset,
        endProgress = endProgress)

//    override def toString = s"BufferWrite.GetN($f, $buf)@${hashCode().toHexString}"
  }

  private[impl] abstract class Impl[T <: synth.Txn[T], Next](protected val blockSize: Int,
                                                       numFrames: Int, offset0: Int, val buf: Buffer, key: String)
    extends ProcessorBase[Unit, Resource] with Buffer.ProxyResource { impl =>

    // --- abstract ---

    protected def prepareNext(offset: Int, chunk: Int, endProgress: Double)(implicit tx: RT): Next

    // --- impl ---

    final type Prod = Unit

    private val offsetRef = Ref(offset0)
    private val stopFrame = offset0 + numFrames

    // ---- processor body ----

    protected final def awaitFut(fut: Future[Any], pr: Double)(loop: => Future[Unit]): Future[Unit] =
      if (fut.isCompleted) {
        progress = pr
        if (progress == 1.0) Future.unit else loop

      } else {
        // check once a second if processor was aborted
        val futTimeOut = Executor.timeOut(fut, 1L, TimeUnit.SECONDS).recover {
          case _: TimeoutException => ()
        }
        futTimeOut.flatMap { _ =>
          checkAborted()
          awaitFut(fut, pr)(loop)
        }
      }

    protected def nextChunk(): Option[Next] =
      atomic { implicit tx =>
        val offset  = offsetRef()
        val chunk   = min(stopFrame - offset, blockSize)
        val stop    = offset + chunk
        if (chunk > 0) {
          offsetRef() = stop
          implicit val ptx: RT = RT.wrap(tx)
          // buf might be offline if dispose was called
          if (buf.isOnline) {
            val pr  = if (stop == stopFrame) 1.0 else min(0.9999, stop.toDouble / stopFrame)
            val res = prepareNext(offset = offset, chunk = chunk, endProgress = pr)
            Some(res)
          } else None
        } else None
      }

    // ----

    def dispose()(implicit tx: RT): Unit =
      tx.afterCommit(abort())
  }
}
