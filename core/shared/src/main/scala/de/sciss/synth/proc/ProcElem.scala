/*
 *  ProcUGens.scala
 *  (SoundProcesses)
 *
 *  Copyright (c) 2010-2025 Hanns Holger Rutz. All rights reserved.
 *
 *	This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.synth.proc

import de.sciss.lucre.expr
import de.sciss.lucre.expr.ExElem
import de.sciss.proc
import de.sciss.synth.UGenSource

/** Registers UGen and Ex graph elements proper to SoundProcesses.  */
object ProcElem {
  private lazy val _init: Unit = {
    UGenSource.addProductReaderSq (synthSq)
    UGenSource.addProductReaders  (synthMap)
    ExElem    .addProductReaderSq (exSq)
    ExElem    .addProductReaderMap(exMap)
  }

  private type ExV = ExElem.ProductReader[Product]

  private def exSq: Seq[ExV] = {
    import expr.graph._
    Seq[ExV](
      AudioCue, AudioCue.Empty, AudioCue.Artifact, AudioCue.Spec, AudioCue.Offset, AudioCue.Gain, AudioCue.FileOffset,
      AudioFileSpec, AudioFileSpec.NumChannels, AudioFileSpec.NumFrames, AudioFileSpec.SampleRate, AudioFileSpec.Read,
      AudioFileSpec.Empty,
      AuralSystem, AuralSystem.ServerSampleRate,
      Bounce,
      Calendar, Calendar.Trunc, Calendar.Set, Calendar.Add, Calendar.Get, Calendar.Schedule,
      Color, Color.Predef, Color.FromHSB, Color.ARGB, Color.Component, Color.Mix,
      Curve.Const, Curve.Par,
      proc.Color.Predefined, proc.Color.User,
      Delay, Delay.Cancel,
      EnvSegment.Single, EnvSegment.Multi, EnvSegment.Start, EnvSegment.Curve,
      FadeSpec, FadeSpec.NumFrames, FadeSpec.Curve, FadeSpec.Floor,
      File.TmpDir, File.MkDir, File.Delete, File.List,
      Grapheme, Grapheme.Add, Grapheme.AddAll, Grapheme.Remove, Grapheme.RemoveAt, Grapheme.Clear,
      Grapheme.FirstEvent, Grapheme.LastEvent, Grapheme.EventBefore, Grapheme.EventAfter, Grapheme.ValueAt,
      Midi.Devices, Midi.Device.Descr, Midi.Device.Empty, Midi.Device.IsInput, Midi.Device.IsOutput,
      Midi.Device.Name, Midi.Device.RegEx, Midi.Device.Vendor,
      Midi.In, Midi.In.ShortMessage, Midi.In.BendMessage, Midi.In.Received,
      Midi.Out, Midi.Out.ShortMessage, Midi.Out.BendMessage, Midi.Out.SysexMessage,
      Midi.Node, Midi.Node.CC, Midi.Node.CC14,
      OscNode.Dump, OscNode.Codec, OscUdpNode, OscUdpNode.Received, OscUdpNode.Sender, OscUdpNode.Message,
      OscUdpNode.Send,
      /* OscPacket: */ OscMessage, OscMessage.Name, OscMessage.Args, OscMessage.Select,
      ParamSpec,
      Proc, Proc.Tape, Proc.Output,
      Runner, Runner.Messages, Runner.Progress, Runner.State, Runner.Stop, Runner.RunWith, Runner.Attr,
      Runner.RunWithAttr, Runner.Run,
      SocketAddress, SocketAddress.LocalHost, SocketAddress.Host, SocketAddress.Port,
      Sys.Process, Sys.Process.Directory, Sys.Process.Output, Sys.Exit, Sys.Property, Sys.Env,
      ThisRunner, ThisRunner.Stop, ThisRunner.Done, ThisRunner.Fail, ThisRunner.Progress,
      ThisRunner.Attr, ThisRunner.Attr.Update, ThisRunner.Attr.UpdateOption, ThisRunner.Attr.Set,
      Timed.Span, Timed.Value,
      Timeline, Timeline.Add, Timeline.AddAll, Timeline.Remove, Timeline.Split, Timeline.Split.Left,
      Timeline.Split.Right, Timeline.Children,
      Warp.Const, Warp.Par,
    )
  }

  private def exMap: Map[String, ExV] = {
    import expr.graph._
    Map[String, ExV](
      ("Midi.Devices", Midi.Devices),
      ("Midi.Device.Descr", Midi.Device.Descr),
      ("Midi.Device.Empty", Midi.Device.Empty),
      ("Midi.Device.IsInput", Midi.Device.IsInput),
      ("Midi.Device.IsOutput", Midi.Device.IsOutput),
      ("Midi.Device.Name", Midi.Device.Name),
      ("Midi.Device.RegEx", Midi.Device.RegEx),
      ("Midi.Device.Vendor", Midi.Device.Vendor),
      ("Midi.In", Midi.In),
      ("Midi.In.ShortMessage", Midi.In.ShortMessage),
      ("Midi.In.Received", Midi.In.Received),
      ("Midi.Out", Midi.Out),
      ("Midi.Out.ShortMessage", Midi.Out.ShortMessage),
      ("Midi.Node", Midi.Node),
      ("Midi.Node.CC", Midi.Node.CC),
      ("Midi.Node.CC14", Midi.Node.CC14),
    ) .map { case (key, value) =>
      (key.replace('.', '$'), value)
    }
  }

  private type SynthV = UGenSource.ProductReader[Product]

  private def synthSq: Seq[SynthV] = {
    import de.sciss.synth.ugen
    import graph._
    Seq[SynthV](
      ugen.IfThen, ugen.IfLagThen, ugen.ElseIfThen, ugen.ElseUnit, ugen.ElseGE, ugen.ThisBranch,
      Action, Action.GetBuf, Action.WriteBuf,
      Reaction,
      Attribute, TrigAttribute,
      Buffer, Buffer.Empty,
      BufferGen,
      BufferOut,
      FadeIn, FadeOut, FadeInOut,
      MkValue,
      Param,
      ScanIn, ScanOut, ScanInFix,
      StopSelf, DoneSelf,
      /* Stream: */ DiskIn, DiskIn.Done, VDiskIn, VDiskIn.Done, DiskOut, BufChannels, BufRateScale, BufSampleRate,
      PartConv,
      Time, Offset, Duration,
    )
  }

  private def synthMap: Map[String, SynthV] = {
    import graph._
    import BufferGen._
    Map(
      (Cheby.readerKey, Command),
      (Copy .readerKey, Command),
      (Sine1.readerKey, Command),
      (Sine2.readerKey, Command),
      (Sine3.readerKey, Command),
    )
  }

  def init(): Unit = _init
}
