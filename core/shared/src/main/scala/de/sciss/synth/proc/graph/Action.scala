/*
 *  Action.scala
 *  (SoundProcesses)
 *
 *  Copyright (c) 2010-2025 Hanns Holger Rutz. All rights reserved.
 *
 *	This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.synth.proc.graph

import de.sciss.audiofile.{AudioFileType, SampleFormat}
import de.sciss.proc.UGenGraphBuilder
import de.sciss.proc.UGenGraphBuilder.Input
import de.sciss.proc.impl.{BufferGet, BufferWrite}
import de.sciss.synth.UGenSource.{ProductReader, RefMapIn}
import de.sciss.synth.ugen
import de.sciss.synth.{ControlRated, GE, HasSideEffect, Lazy, UGenInLike}

import scala.annotation.switch

/** A graph element that executes an action upon receiving a trigger.
  */
object Action extends ProductReader[Action] {
  override def read(in: RefMapIn, prefix: String, arity: Int): Action = {
    require (arity == 2)
    val _trig = in.readGE()
    val _key  = in.readString()
    new Action(_trig, _key)
  }

  object WriteBuf extends ProductReader[WriteBuf] {
    override def read(in: RefMapIn, key: String, arity: Int): WriteBuf = {
      require (arity == 7)
      val _trig         = in.readGE()
      val _key          = in.readString()
      val _buf          = in.readGE()
      val _numFrames    = in.readGE()
      val _startFrame   = in.readGE()
      val _fileType     = in.readGE()
      val _sampleFormat = in.readGE()
      new WriteBuf(_trig, _key, _buf, _numFrames, _startFrame, _fileType, _sampleFormat)
    }

    /** Converts an audio file type to a unique id that can be parsed by the UGen. */
    def id(in: AudioFileType): Int = in match {
      case AudioFileType.AIFF    => 0
      case AudioFileType.Wave    => 1
      case AudioFileType.Wave64  => 2
      case AudioFileType.IRCAM   => 3
      case AudioFileType.NeXT    => 4
      case AudioFileType.Raw     => 5
      case other => sys.error(s"Unexpected audio file type $other")
    }

    /** Converts a sample format to a unique id that can be parsed by the UGen. */
    def id(in: SampleFormat): Int = in match {
      case SampleFormat.Int16    => 0
      case SampleFormat.Int24    => 1
      case SampleFormat.Float    => 2
      case SampleFormat.Int32    => 3
      case SampleFormat.Int8     => 4
      case other => sys.error(s"Unexpected sample format $other")
    }

    /** Recovers an audio file type from an id. Throws an exception if the id is invalid. */
    def fileType(id: Int): AudioFileType = (id: @switch) match {
      case 0 => AudioFileType.AIFF
      case 1 => AudioFileType.Wave
      case 2 => AudioFileType.Wave64
      case 3 => AudioFileType.IRCAM
      case 4 => AudioFileType.NeXT
      case 5 => AudioFileType.Raw
      case other => sys.error(s"Unexpected audio file type id $other")
    }

    def maxFileTypeId: Int = 5

    /** Recovers a sample format from an id. Throws an exception if the id is invalid. */
    def sampleFormat(id: Int): SampleFormat = (id: @switch) match {
      case 0 => SampleFormat.Int16
      case 1 => SampleFormat.Int24
      case 2 => SampleFormat.Float
      case 3 => SampleFormat.Int32
      case 4 => SampleFormat.Int8
      case other => sys.error(s"Unexpected sample format id $other")
    }

    def maxSampleFormatId: Int = 4
  }

  /** A graph element that when triggered writes the contents of a buffer to an audio file.
    * The writing happens asynchronously, and the element outputs a trigger itself when the writing
    * has completed.
    *
    * '''Note''': currently, the behaviour is undefined if another trigger is received before a previous
    * writing process finishes. The caller should also avoid stopping the enclosing `Proc` before the
    * writing process finishes (there is no guarantee that the buffer contents is preserved).
    *
    * An alternative is `BufferOut` which is slightly less flexible. It writes the buffer contents when
    * the process finishes, and invokes a done-action.
    *
    * @param trig         a trigger signal that initiate the write process.
    * @param key          the key into the enclosing object's attribute map, where the output `Artifact`
    *                     is to be defined
    * @param buf          the identifier of the buffer to write
    * @param numFrames    the number of frames to write or `-1` (default) to write as many frames as possible
    * @param startFrame   the offset into the buffer to start writing from, which defaults to zero.
    * @param fileType     `-1` automatic (default; determined from artifact found), `0` AIFF, `1` Wave, `2` Wave64,
    *                     `3` IRCAM, `4` NeXT, `5` Raw
    * @param sampleFormat `0` 16-bit integer, `1` 24-bit integer, `2` 32-bit floating point (default),
    *                     `3` 32-bit integer, `4` 8-bit integer
    *
    * @see [[BufferOut]]
    */
  final case class WriteBuf(trig: GE, key: String, buf: GE, numFrames: GE = -1, startFrame: GE = 0,
                            fileType: GE = -1, sampleFormat: GE = 2) extends GE.Lazy with ControlRated {

    override def productPrefix = s"Action$$WriteBuf"  // for serialization

    override protected def makeUGens: UGenInLike = {
      val b = UGenGraphBuilder.get
      b.requestInput(Input.BufferWrite(key))
      BufferWrite.makeUGen(this)
    }
  }

  object GetBuf extends ProductReader[GetBuf] {
    override def read(in: RefMapIn, key: String, arity: Int): GetBuf = {
      require (arity == 5)
      val _trig         = in.readGE()
      val _key          = in.readString()
      val _buf          = in.readGE()
      val _numFrames    = in.readGE()
      val _startFrame   = in.readGE()
      new GetBuf(_trig, _key, _buf, _numFrames, _startFrame)
    }
  }

  /** A graph element that when triggered sends the contents of a buffer to the client.
    * The transfer happens asynchronously, and the element outputs a trigger itself when the transfer
    * has completed.
    *
    * '''Note''': currently, the behaviour is undefined if another trigger is received before a previous
    * transfer process finishes. The caller should also avoid stopping the enclosing `Proc` before the
    * transfer process finishes (there is no guarantee that the buffer contents is preserved).
    *
    * @param trig         a trigger signal that initiate the transfer process.
    * @param key          the key into the enclosing object's attribute map, where a variable of type `Seq[Double]`
    *                     is to be found
    * @param buf          the identifier of the buffer to transfer
    * @param numFrames    the number of frames to transfer or `-1` (default) to transfer as many frames as possible
    * @param startFrame   the offset into the buffer to start transferring from, which defaults to zero.
    *
    * @see [[WriteBuf]]
    */
  final case class GetBuf(trig: GE, key: String, buf: GE, numFrames: GE = -1, startFrame: GE = 0)
    extends GE.Lazy with ControlRated {

    override def productPrefix = s"Action$$GetBuf"  // for serialization

    override protected def makeUGens: UGenInLike = {
      val b = UGenGraphBuilder.get
      b.requestInput(Input.BufferGet(key))
      BufferGet.makeUGen(this)
    }
  }
}
/** A graph element that executes an action upon receiving a trigger.
  *
  * @param trig   the trigger input signal
  * @param key    a key into the process' attribute map or the caller's context map.
  *               The value peer stored at that location should be of type `proc.Action`. Alternatively,
  *               if the process is started by a control program, an instance of `Act`
  *               can be passed via `runWith`.
  */
final case class Action(trig: GE, key: String) extends Lazy.Expander[Unit] with HasSideEffect {
  protected def makeUGens: Unit = {
    val b = UGenGraphBuilder.get
    b.requestInput(Input.Action(key))
    impl.ActionResponder.makeUGen(trig, None, key)
  }
}

object Reaction extends ProductReader[Reaction] {
  override def read(in: RefMapIn, prefix: String, arity: Int): Reaction = {
    require (arity == 3)
    val _trig = in.readGE()
    val _in   = in.readGE()
    val _key  = in.readString()
    new Reaction(_trig, _in, _key)
  }
}
/** A graph element that executes an action upon receiving a trigger,
  * sampling the values at that moment and making them available
  * in the action through the `values` method.
  *
  * @param trig   the trigger input signal
  * @param in     the input signal to sample and pass on to the action
  * @param key    a key into the process' attribute map. the value peer stored
  *               at that location should be of type `proc.Action`
  */
final case class Reaction(trig: GE, in: GE, key: String) extends Lazy.Expander[Unit] with HasSideEffect {
  protected def makeUGens: Unit = {
    val b = UGenGraphBuilder.get
    b.requestInput(Input.Action(key))
    impl.ActionResponder.makeUGen(trig, Some(ugen.Flatten(in)), key)
  }
}
