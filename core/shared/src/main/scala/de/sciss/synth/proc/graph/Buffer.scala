/*
 *  Buffer.scala
 *  (SoundProcesses)
 *
 *  Copyright (c) 2010-2025 Hanns Holger Rutz. All rights reserved.
 *
 *	This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.synth.proc.graph

import de.sciss.proc.UGenGraphBuilder
import de.sciss.proc.UGenGraphBuilder.Input
import de.sciss.synth.UGenSource.{ProductReader, RefMapIn}
import de.sciss.synth.ugen.ControlProxy
import de.sciss.synth.{GE, Rate, ResolveGE, ScalarRated, UGenInLike, control, scalar}

import scala.collection.immutable.{IndexedSeq => Vec}

/** An element referring to a random access buffer provided through an attribute.
  * The attribute will typically be an audio grapheme.
  */
object Buffer extends ProductReader[Buffer] {
  def ir(key: String): Buffer = new Buffer(scalar , key)
  def kr(key: String): Buffer = new Buffer(control, key)

  /** Convenience alias for `kr` */
  def apply(key: String): Buffer = kr(key)

  /* private[proc] */ def controlName(key: String): String = s"$$buf_$key"

  override def read(in: RefMapIn, prefix: String, arity: Int): Buffer = {
    require (arity == 2)
    val _rate     = in.readRate()
    val _key      = in.readString()
    new Buffer(_rate, _key)
  }

  object Empty extends ProductReader[Empty] {
    def controlName(id: Int): String = s"$$buf_emp$id"

    override def read(in: RefMapIn, prefix: String, arity: Int): Empty = {
      require (arity == 2)
      val _numFrames    = in.readGE()
      val _numChannels  = in.readGE()
      new Empty(_numFrames, _numChannels)
    }
  }

  /** Creates an empty buffer.
    *
    * @param numFrames      the number of frames for the buffer.
    *                       Must be resolvable at graph expansion time.
    * @param numChannels    the number of channels for the buffer (defaults to `1`).
    *                       Must be resolvable at graph expansion time.
    */
  final case class Empty(numFrames: GE, numChannels: GE = 1) extends GE.Lazy with ScalarRated {

    override def productPrefix = s"Buffer$$Empty"  // for serialization

    private def fail(arg: String, detail: String): Nothing =
      throw new IllegalArgumentException(s"Empty.$arg cannot be resolved at initialization time: $detail")

    ResolveGE.test(numFrames  ).left.foreach(fail("numFrames"  , _))
    ResolveGE.test(numChannels).left.foreach(fail("numChannels", _))

    protected def makeUGens: UGenInLike = {
      val b             = UGenGraphBuilder.get
      val numFramesI    = ResolveGE.float(numFrames  , b).fold[Float](fail("numFrames"  , _), identity).toInt
      val numChannelsI  = ResolveGE.float(numChannels, b).fold[Float](fail("numChannels", _), identity).toInt

      val value         = b.requestInput(Input.BufferEmpty(numFrames = numFramesI, numChannels = numChannelsI))
      val ctlName       = Empty.controlName(value.id)
      val ctl           = ControlProxy(rate, Vector(0f), Some(ctlName))
      ctl.expand
    }
  }
}

/** An element referring to a random access buffer provided through an attribute.
  * The attribute will typically be an audio grapheme.
  *
  * @param key  the attribute key.
  */
final case class Buffer(rate: Rate, key: String) extends GE.Lazy {
  protected def makeUGens: UGenInLike = {
    val b       = UGenGraphBuilder.get
    b.requestInput(Input.Buffer(key))
    val ctlName = Buffer.controlName(key)
    val ctl     = ControlProxy(rate, Vec(0f), Some(ctlName))
    ctl.expand
  }
}