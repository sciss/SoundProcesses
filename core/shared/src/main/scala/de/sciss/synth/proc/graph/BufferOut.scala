/*
 *  BufferOut.scala
 *  (SoundProcesses)
 *
 *  Copyright (c) 2010-2025 Hanns Holger Rutz. All rights reserved.
 *
 *	This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.synth.proc.graph

import de.sciss.proc.UGenGraphBuilder
import de.sciss.proc.UGenGraphBuilder.Input
import de.sciss.synth.UGenSource.{ProductReader, RefMapIn}
import de.sciss.synth.ugen.ControlProxy
import de.sciss.synth.{GE, Rate, ResolveGE, UGenInLike, control, scalar}

// XXX TODO --- should be Buffer.Out
object BufferOut extends ProductReader[BufferOut] {
  def ir(artifact: String, action: String, numFrames: GE, numChannels: GE = 1): BufferOut =
    new BufferOut(scalar , artifact, numFrames = numFrames, numChannels = numChannels, action = action)
  
  def kr(artifact: String, action: String, numFrames: GE, numChannels: GE = 1): BufferOut =
    new BufferOut(control, artifact, numFrames = numFrames, numChannels = numChannels, action = action)

  /** Convenience alias for `kr` */
  def apply(artifact: String, action: String, numFrames: GE, numChannels: GE = 1): BufferOut =
    kr(artifact, numFrames = numFrames, numChannels = numChannels, action = action)

  /* private[proc] */ def controlName(artifact: String, action: String): String = s"$$buf_${artifact}_$action"

  override def read(in: RefMapIn, prefix: String, arity: Int): BufferOut = {
    require (arity == 5)
    val _rate         = in.readRate()
    val _artifact     = in.readString()
    val _action       = in.readString()
    val _numFrames    = in.readGE()
    val _numChannels  = in.readGE()
    new BufferOut(_rate, _artifact, _action, _numFrames, _numChannels)
  }
}
/** A graph element that creates an empty buffer for the synth graph to write to. Upon completion
  * of the encompassing proc, the buffer contents is written to an artifact referred to by its
  * attribute-map key. When the file has been written, the action referred to by its attribute-map
  * key is called. The element outputs the buffer-id.
  *
  * An alternative is `Action.WriteBuf` which is slightly more flexible. It writes the buffer contents when
  * a trigger is received, and outputs another trigger when writing finishes.
  *
  * @param rate         the rate at which the buffer-id is presented
  * @param artifact     a key into the encompassing object's attribute map, leading to an `Artifact`.
  * @param action       a key into the encompassing object's attribute map, leading to an `Action`.
  * @param numFrames    the number of frames to allocate
  * @param numChannels  the number of channels to allocate
  *
  * @see [[Action.WriteBuf]]
  */
final case class BufferOut(rate: Rate, artifact: String, action: String, numFrames: GE, numChannels: GE)
  extends GE.Lazy {

  private def fail(arg: String, detail: String): Nothing =
    throw new IllegalArgumentException(s"BufferOut.$arg cannot be resolved at initialization time: $detail")

  ResolveGE.test(numFrames  ).left.foreach(fail("numFrames"  , _))
  ResolveGE.test(numChannels).left.foreach(fail("numChannels", _))

  protected def makeUGens: UGenInLike = {
    val b             = UGenGraphBuilder.get
    val numFramesI    = ResolveGE.float(numFrames  , b).fold[Float](fail("numFrames"  , _), identity).toInt
    val numChannelsI  = ResolveGE.float(numChannels, b).fold[Float](fail("numChannels", _), identity).toInt

    b.requestInput(Input.BufferOut(artifact = artifact, action = action,
      numFrames = numFramesI, numChannels = numChannelsI))
    val ctlName       = BufferOut.controlName(artifact = artifact, action = action)
    val ctl           = ControlProxy(rate, Vector(0f), Some(ctlName))
    ctl.expand
  }
}