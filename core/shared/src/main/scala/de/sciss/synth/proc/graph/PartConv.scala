/*
 *  PartConv.scala
 *  (SoundProcesses)
 *
 *  Copyright (c) 2010-2025 Hanns Holger Rutz. All rights reserved.
 *
 *	This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.synth.proc.graph

import de.sciss.proc.{UGenGraphBuilder => UGB}
import de.sciss.synth.UGenSource.{ProductReader, RefMapIn, Vec}
import de.sciss.synth.{GE, Rate, ResolveGE, UGenInLike, audio, scalar, ugen}

object PartConv extends ProductReader[PartConv] {
  /** A SoundProcesses aware variant of `PartConv`, a UGen for partitioned convolution. It takes its buffer input from
    * an attribute with the given `key`. Partitioned convolution breaks up a long impulse response into piece,
    * allowing them to very large. CPU load increases with IR size, and there are tradeoffs based on `fftSize` choice,
    * due to rarer but larger FFTs. Amortisation to used to spread processing and avoid spikes.
    *
    * The buffer contents must be monophonic. If multiple channels are desired, they must be
    * individually provided to separate `PartConv` instances.
    *
    * @param key      key into the containing object's attribute map, where an `AudioCue` is to be found.
    * @param fftSize  the size of the internal fft used during partitioned convolution.
    *                 the default is `2048`, but 1024 and higher powers-of-two should be possible.
    *                 Must be resolvable at graph expansion time.
    */
  def ar(key: String, in: GE, fftSize: GE = 2048): PartConv = {
    apply(audio, key, in = in, fftSize = fftSize)
  }

  // def controlName(id: Int): String = s"$$buf_cnv$id"
  def controlName(key: String): String = s"$$cnv_$key"

  override def read(in: RefMapIn, prefix: String, arity: Int): PartConv = {
    require (arity == 4)
    val _rate     = in.readRate()
    val _key      = in.readString()
    val _in       = in.readGE()
    val _fftSize  = in.readGE()
    new PartConv(_rate, _key, _in, _fftSize)
  }
}

/** A SoundProcesses aware variant of `PartConv`, a UGen for partitioned convolution. It takes its buffer input from
  * an attribute with the given `key`. Partitioned convolution breaks up a long impulse response into piece,
  * allowing them to very large. CPU load increases with IR size, and there are tradeoffs based on `fftSize` choice,
  * due to rarer but larger FFTs. Amortisation to used to spread processing and avoid spikes.
  *
  * The buffer contents must be monophonic. If multiple channels are desired, they must be
  * individually provided to separate `PartConv` instances.
  *
  * @param key      key into the containing object's attribute map, where an `AudioCue` is to be found.
  * @param fftSize  the size of the internal fft used during partitioned convolution.
  *                 the default is `2048`, but 1024 and higher powers-of-two should be possible.
  *                 Must be resolvable at graph expansion time.
  */
final case class PartConv(rate: Rate, key: String, in: GE, fftSize: GE) extends GE.Lazy {

  private def fail(detail: String): Nothing =
    throw new IllegalArgumentException(s"PartConv.fftSize cannot be resolved at initialization time: $detail")

  ResolveGE.test(fftSize).left.foreach(fail)

  protected def makeUGens: UGenInLike = {
    val b         = UGB.get
    val fftSizeI  = ResolveGE.float(fftSize, b).fold[Float](fail, identity).toInt
    b.requestInput(UGB.Input.PartConv(key, fftSize = fftSizeI))
    val ctlName   = PartConv.controlName(key)
    val buf       = ugen.ControlProxy(scalar, Vec(0f), Some(ctlName))
    ugen.PartConv(rate, in = in, fftSize = fftSize, buf = buf)
  }
}
