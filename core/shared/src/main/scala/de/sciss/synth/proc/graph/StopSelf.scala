/*
 *  StopSelf.scala
 *  (SoundProcesses)
 *
 *  Copyright (c) 2010-2025 Hanns Holger Rutz. All rights reserved.
 *
 *	This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.synth.proc.graph

import de.sciss.proc.UGenGraphBuilder
import de.sciss.proc.UGenGraphBuilder.Input
import de.sciss.synth.UGenSource.{ProductReader, RefMapIn}
import de.sciss.synth.{GE, Lazy, ugen}

object StopSelf extends ProductReader[StopSelf] {
  final val replyName = "/$stop"

  override def read(in: RefMapIn, prefix: String, arity: Int): StopSelf = {
    require (arity == 2)
    val _trig   = in.readGE()
    val _pause  = in.readGE()
    new StopSelf(_trig, _pause)
  }
}

/** A UGen that when triggers stops the encompassing synth, marking
  * the runner as `stopped`.
  * As opposed to `FreeSelf`, this triggers a disposal action on
  * the client side, possibly synchronising the disposal of
  * associated resources.
  *
  * When there is a need to distinguish a runner's `stop` from `done`,
  * the `DoneSelf` UGen can be used.
  *
  * @param trig   the trigger signal that invokes the disposal
  * @param pause  if non-zero, immediately pauses the synth
  *               when the trigger is received. This can be useful
  *               because the client may take a moment to actually
  *               dispose the synth.
  *
  * @see [[DoneSelf]]
  */
final case class StopSelf(trig: GE, pause: GE = 1) extends Lazy.Expander[Unit] {
  protected def makeUGens: Unit = {
    import ugen._
    import de.sciss.synth.Import._
    PauseSelf.kr(trig & pause)
    val b = UGenGraphBuilder.get
    b.requestInput(Input.StopSelf(done = false))
    SendReply(trig.rate, trig, values = 0, msgName = StopSelf.replyName)
    ()
  }
}

object DoneSelf extends ProductReader[DoneSelf] {
  final val replyName = "/$done"

  override def read(in: RefMapIn, prefix: String, arity: Int): DoneSelf = {
    require (arity == 2)
    val _trig   = in.readGE()
    val _pause  = in.readGE()
    new DoneSelf(_trig, _pause)
  }
}

/** A UGen that when triggers stops the encompassing synth, marking
  * the runner as `done`.
  * As opposed to `FreeSelf`, this triggers a disposal action on
  * the client side, possibly synchronising the disposal of
  * associated resources.
  *
  * @param trig   the trigger signal that invokes the disposal
  * @param pause  if non-zero, immediately pauses the synth
  *               when the trigger is received. This can be useful
  *               because the client may take a moment to actually
  *               dispose the synth.
  *
  * @see [[StopSelf]]
  */
final case class DoneSelf(trig: GE, pause: GE = 1) extends Lazy.Expander[Unit] {
  protected def makeUGens: Unit = {
    import ugen._
    import de.sciss.synth.Import._
    PauseSelf.kr(trig & pause)
    val b = UGenGraphBuilder.get
    b.requestInput(Input.StopSelf(done = true))
    SendReply(trig.rate, trig, values = 0, msgName = DoneSelf.replyName)
    ()
  }
}
