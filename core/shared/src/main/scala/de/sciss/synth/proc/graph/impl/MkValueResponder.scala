/*
 *  MkValueResponder.scala
 *  (SoundProcesses)
 *
 *  Copyright (c) 2010-2025 Hanns Holger Rutz. All rights reserved.
 *
 *	This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.synth.proc.graph.impl

import de.sciss.lucre.expr.graph.{Const => ExConst, Var => ExVar}
import de.sciss.lucre.synth.{RT, Synth, Txn}
import de.sciss.lucre.{Expr, Source, Txn => LTxn, Var => LVar}
import de.sciss.proc.{AuralContext, SoundProcesses}
import de.sciss.synth.GE
import de.sciss.{osc, synth}

import scala.collection.immutable.{IndexedSeq => Vec}

object MkValueResponder {
  // via SendReply
  private def replyName(key: String): String = s"/$$mk_$key"

  def makeUGen(trig: GE, values: GE, key: String): Unit = {
    import synth._
    import ugen._
    SendReply.kr(trig = trig, values = values, msgName = replyName(key), id = 0)
    ()
  }

  var DEBUG = false

  final class WithEx[T <: Txn[T], A](vr: ExVar.Expanded[T, A], key: String, synth: Synth)
                                 (implicit context: AuralContext[T])
    extends MkValueResponder(key, synth) {

    override protected def trySet(value: Double     )(implicit tx: T): Unit = trySetAny(value)
    override protected def trySet(value: Vec[Double])(implicit tx: T): Unit = trySetAny(value)

    private def trySetAny(value: Any)(implicit tx: T): Unit =
      vr.fromAny.fromAny(value).foreach { valueT =>
        vr.update(new ExConst.Expanded[T, A](valueT))
      }
  }

  final class WithExpr[T <: Txn[T], A, Repr[~ <: LTxn[~]] <: Expr[~, A]](vrH: Source[T, LVar[T, Repr[T]]],
                                                                         key: String, synth: Synth)
                                                                        (lift: Double => A)
                                                                        (implicit context: AuralContext[T], tpe: Expr.Type[A, Repr])
    extends MkValueResponder(key, synth) {

    override protected def trySet(value: Double)(implicit tx: T): Unit = {
      val c   = tpe.newConst[T](lift(value))
      val vr  = vrH()
      vr.update(c)
    }

    override protected def trySet(value: Vec[Double])(implicit tx: T): Unit = ()
  }

  final class WithVec[T <: Txn[T], A, Repr[~ <: LTxn[~]] <: Expr[~, Vec[A]]](vrH: Source[T, LVar[T, Repr[T]]],
                                                                             key: String, synth: Synth)
                                                                            (lift: Vec[Double] => Vec[A])
                                                                            (implicit context: AuralContext[T], tpe: Expr.Type[Vec[A], Repr])
    extends MkValueResponder(key, synth) {

    override protected def trySet(value: Double)(implicit tx: T): Unit = trySet(Vector(value))

    override protected def trySet(value: Vec[Double])(implicit tx: T): Unit = {
      val c   = tpe.newConst[T](lift(value))
      val vr  = vrH()
      vr.update(c)
    }
  }
}
abstract class MkValueResponder[T <: Txn[T]](key: String, protected val synth: Synth)
                                            (implicit context: AuralContext[T])
  extends SendReplyResponder {

  import MkValueResponder._

  private[this] val Name    = replyName(key)
  private[this] val NodeId  = synth.peer.id

  protected def trySet(value: Double)(implicit tx: T): Unit
  protected def trySet(value: Vec[Double])(implicit tx: T): Unit

  private def stepTrySet(value: Double): Unit = {
    import context.universe.cursor
    SoundProcesses.step[T](s"MkValueResponder($synth, $key)") { implicit tx: T =>
      trySet(value)
    }
  }

  private def stepTrySet(value: Vec[Double]): Unit = {
    import context.universe.cursor
    SoundProcesses.step[T](s"MkValueResponder($synth, $key)") { implicit tx: T =>
      trySet(value)
    }
  }

  protected val body: Body = {
    case osc.Message(Name, NodeId, 0, v0: Float) =>
      stepTrySet(v0.toDouble)
    case osc.Message(Name, NodeId, 0, raw @ _*) =>
      if (DEBUG) println(s"MkValueResponder($key, $NodeId) - received trigger")
      // logAural(m.toString)
      val values: Vec[Double] = raw match {
        case rawV: Vec[Any] =>
          rawV.collect {
            case f: Float => f.toDouble
          }

        case _ =>
          raw.iterator.collect {
            case f: Float => f.toDouble
          }.toIndexedSeq
      }
      stepTrySet(values)
  }

  protected def added()(implicit tx: RT): Unit = ()
}