/*
 *  TriggerControl.scala
 *  (SoundProcesses)
 *
 *  Copyright (c) 2010-2025 Hanns Holger Rutz. All rights reserved.
 *
 *	This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.synth.proc.graph.impl

import de.sciss.lucre.Txn.peer
import de.sciss.lucre.expr.ITrigger
import de.sciss.lucre.synth.{DynamicUser, RT, Txn}
import de.sciss.proc.AuralNode
import de.sciss.synth.proc.graph.TrigAttribute

import scala.concurrent.stm.Ref

class TriggerControl[T <: Txn[T]](trigger: ITrigger[T], key: String, auralNode: AuralNode[T], tx0: T)
                    /*(implicit context: AuralContext[T])*/ extends DynamicUser {

  private[this] val ctlName = TrigAttribute.controlName(key)

  // ---- constructor ----

  private[this] val obs = trigger.changed.react { implicit tx => _ =>
    if (added()) auralNode.synth.set(ctlName -> 1f)
  } (tx0)

  auralNode.addDisposable(obs)(tx0)

  private[this] val added = Ref(false)

  override def add    ()(implicit tx: RT): Unit = added() = true
  override def remove ()(implicit tx: RT): Unit = added() = false
}
