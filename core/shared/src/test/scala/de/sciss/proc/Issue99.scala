package de.sciss.proc

import de.sciss.serial.{DataInput, DataOutput}
import org.scalatest.funspec.AnyFunSpec

/*
  To run only this suite:

  testOnly de.sciss.proc.Issue99

 */
class Issue99 extends AnyFunSpec {
  SoundProcesses.init()

  describe("An Act graph with Osc sending") {
    it("should serialize and de-serialize") {
      val out = DataOutput()
      val graph: Action.Graph = Action.Graph {
        import de.sciss.lucre.expr.graph._
        val osc = OscUdpNode(57121)
        val tgt = SocketAddress(port = 57120, host = "127.0.0.1")
        osc.send(tgt, OscMessage("/foo"))
      }
      Action.GraphObj.valueFormat.write(graph, out)
      val in  = DataInput(out.toByteArray)
      val res = Action.GraphObj.valueFormat.read(in)
      assert(res === graph)
    }
  }
}