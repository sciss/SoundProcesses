# MIDI Control

Make it somewhat similar to `OscUdpNode` and `GPIO.DigitalIn`.

```scala
// val m = Midi.In("EC4*")
val devs = Midi.Devices()
val m = devs.collectFirst { d => d.name.contains("EC4") && d.isInput } .getOrElse(Midi.Info.Dummy)

LoadBang() --> devs.foreach(PrintLn)    // does `foreach` work already this way? or `map`?

val vr = Var(0)
m.cc(chan = Quote(0), num = Quote(100), value = vr) --> PrintLn("CC is " ++ vr.toStr)

Runner("bla").runWith("cc" -> (vr / 127.0))

object Midi {
  // easy construction using reg-ex
  def In(name: Ex[String] = "*", descr: Ex[String] = "*", vendor: Ex[String] = "*"): In

  // generic construction
  def In(info: Ex[Info]): In
  
  def Devices(): Ex[Seq[Info]]
  
  object Info {
    def Dummy: Ex[Info] // or `Empty`?
  
    implicit class Ops(in: Ex[Info]) {
      def name  : Ex[String]
      def descr : Ex[String]
      def vendor: Ex[String]
    }
  }
  trait Info {
    def name    : String
    def descr   : String
    def vendor  : String
    
    def isInput : Boolean
    def isOutput: Boolean
  }
  
  trait In extends Control {
    def cc    (chan: CaseDef[Int], num  : CaseDef[Int], value : CaseDef[Int]): Trig
    def noteOn(chan: CaseDef[Int], pitch: CaseDef[Int], volume: CaseDef[Int]): Trig
    
    def dump: Ex[Int]
    def dump_=(x: Ex[Int]): Unit
  }
}
```

Should we add a 'model' variant for automatic synchronisation, like a widget?

```scala
val m = Midi.Node(name = "EC4") // reg-ex device matching
val v = m.cc(chan = 0, num = 100)
v.changed --> PrintLn("CC is " ++ v.toStr)
v <-> "value".attr(0)

Runner("bla").runWith("cc" -> (v / 127.0))
```

Since `Node` would imply a combination of input and output device, this could also allow
`m.out.noteOn`, `m.in.noteOn` etc.

