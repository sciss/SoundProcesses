# Writing Buffers (210503)

We already have `DiskOut.ar(key, in)` for "streaming" recordings. We lack a good way to transfer a large
buffer to an output file. The relevant object could be either an `Act` in an expression program, 
or it could be a `Lazy` in a synth program. In the case of `Act`, the disadvantage is that we do not have
an "object" representing a buffer; we have the buffer's input as the synth prepares, coming for example
from an `AudioCue`, but it could also be initially empty. It would be a `Runner` ("future") like interface:

```
val p = "proc".attr(Proc())
val pr = p.runWith()
val b = p.buffer("key")
val art = "out".attr(Artifact())
val a = Act(b.write(art))
??? ---> a
```

Since the action would in any case be asynchronous, it seems easier and more straight forward, to put
the new object into the synth language instead:

```
val b = Buffer("in")
val x: GE = ???
val r = RecordBuf.ar(x, b)
val d = TDiskOut("out", b, Done.kr(r)) // good name?
Action(Done.kr(d), "okay")
```

Avoid names like `BufferWrite`, which can be confusing in comparison with `BufWr` etc.
It could also be `ActDiskOut` or `ActWriteBuf`, opening a possibility to introduce other
asynchronous actions in the future

```
val d = ActWriteBuf("out", b, Done.kr(r))
```
Look reasonable? This will have very low risk of shadowing a future UGen. Alternatively,
`Action.WriteBuffer("out", b, tr)`. That way we would have a dedicated "name space". To align
the arguments with `Action.apply`: `Action.WriteBuffer(tr, b, "out")`. But then it does not
align with `DiskOut`...

The `Artifact` found in attribute `"out"` could even be dynamic, and would be picked up
at the moment the trigger "occurs" (when seen by the client in a `SendReply`). In the first
implementation, we could omit the feature, and implement it later.

```
val b = Buffer("in")
val x = WhiteNoise.ar(0.2)
val r = RecordBuf.ar(x, b)
val d = Action.WriteBuf(Done.kr(r), "out", b, numFrames = -1, startFrame = 0, fileType = -1, sampleFormat = 2)
Action(d, "okay")
```

----

## implementation

We need the buffer information, such as number of frames. We could restrict the buffer argument to be something
that comes out of `Buffer("key")`, but the disadvantage is that it won't allow more intricate things like
`Select.kr` to dynamically select a buffer.

It might thus be better to accept that the operation is already asynchronous, and run a `b_query` to obtain
a fresh buffer wrapper.

 - A trigger occurs. The underlying `SendReply` sends buffer id, num-frames, start-frame, file-type, and
   sample-format values.
 - The reverse analogue to `BufferPrepare` runs; i.e. writing chunk by chunk. This works only in the browser,
   because `b_write` itself cannot append. So this is nasty with large buffers on the desktop? In other words,
   the desktop would also have to fall back to `b_getn` logic and piecing the file together in the client.
 - Once completed, an `n_set` is produced for a synthetic trigger control, which is the signal returned by
   `Action.WriteBuf`.
   
During the asynchronous operation, the node must be monitored; if it is prematurely disposed, stop the transferal
and abort the asynchronous process.

Both `BufferPrepare` and `BufferWrite` will be very slow in the browser. For in|fibrillae, that could mean
we have to "switch spaces" immediately, and simply add the neighbouring reverberation when "it's ready"; and
likewise, keep the old buffer writing even after "switching spaces", therefore not begin recording the _new space_
before the old space has finished writing.

## notes 210504

So `BufferPrepare` is actually very fast, and thus `BufferWrite` which will be similarly constructed, is also
likely to be fast.
