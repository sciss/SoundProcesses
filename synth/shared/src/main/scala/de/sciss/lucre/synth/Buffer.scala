/*
 *  Buffer.scala
 *  (SoundProcesses)
 *
 *  Copyright (c) 2010-2025 Hanns Holger Rutz. All rights reserved.
 *
 *	This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.lucre.synth

import de.sciss.lucre.synth.Resource.TimeStamp
import de.sciss.synth.{Buffer => SBuffer}
import de.sciss.audiofile.{AudioFileType, SampleFormat}
import de.sciss.lucre.synth.impl.BufferImpl
import de.sciss.osc
import de.sciss.synth.message.BufferGen

import scala.concurrent.ExecutionContext

object Buffer {
  private var cueBufSz = 32768
  private var recBufSz = 32768

  def defaultCueBufferSize: Int = cueBufSz
  def defaultCueBufferSize_=(value: Int): Unit = {
    validateCueBufferSize(64, value)
    cueBufSz = value
  }

  def defaultRecBufferSize: Int = recBufSz
  def defaultRecBufferSize_=(value: Int): Unit = {
    validateCueBufferSize(64, value)
    recBufSz = value
  }

  private def isPowerOfTwo(value: Int) = (value & (value - 1)) == 0

  private def validateCueBufferSize(server: Server, value: Int): Unit =
    validateCueBufferSize(server.config.blockSize, value)

  private def validateCueBufferSize(minSize: Int, value: Int): Unit = {
    require(isPowerOfTwo(value) && value >= minSize && value <= 131072,
      s"Must be a power of two and in ($minSize, 131072): $value")
  }

  trait ProxyResource extends Resource /*with Proxy*/ {
    def buf: Buffer

    def isOnline(implicit tx: RT): Boolean = buf.isOnline

    private[synth] def timeStamp                    (implicit tx: RT): TimeStamp = buf.timeStamp
    private[synth] def timeStamp_=(value: TimeStamp)(implicit tx: RT): Unit      = buf.timeStamp = value

    override def toString: String = buf.toString

    def server: Server = buf.server
  }

  /** Utility resource creation that defers buffer disposal until point where node has been freed. */
  def disposeWithNode(buf: Buffer, nr: NodeRef): Resource = {
    val buf0 = buf
    new ProxyResource {
      val buf: Buffer = buf0

      def dispose()(implicit tx: RT): Unit = nr.node.onEndTxn { implicit tx =>
        // println(s"disposeWithNode($buf, $nr)")
        buf.dispose()
      }
    }
  }

  /** Writes the buffer content to a file when the node ends.
    * The caller is responsible for eventually freeing the buffer!
    * This is because we do not open a transaction after writing completes.
    */
  def writeWithNode(buf: Buffer, nr: NodeRef, path: String)(action: => Unit)
                   (implicit exec: ExecutionContext): Resource = {
    val buf0 = buf
    new ProxyResource {
      val buf: Buffer = buf0

      def dispose()(implicit tx: RT): Unit = {
        nr.node.onEnd {
          val fut = buf.server.!!(osc.Bundle.now(buf.peer.writeMsg(path = path)))
          fut.foreach { _ =>
            action
          }
        }
      }
    }
  }

  def diskIn(server: Server)(path: String, startFrame: Long = 0L, numFrames: Int = defaultCueBufferSize,
                             numChannels: Int = 1)(implicit tx: RT): Buffer = {
    validateCueBufferSize(server, numFrames)
    val res = create(server, numFrames = numFrames, numChannels = numChannels, closeOnDisposal = true)
    // res.allocRead(path, startFrame = startFrame, numFrames = numFrames)
    res.alloc()
    res.cue(path, fileStartFrame = startFrame)
    res
  }

  def diskOut(server: Server)(path: String, fileType: AudioFileType = AudioFileType.AIFF,
                              sampleFormat: SampleFormat = SampleFormat.Float,
                              numFrames: Int = defaultRecBufferSize, numChannels: Int = 1)(implicit tx: RT): Buffer = {
    validateCueBufferSize(server, numFrames)
    val res = create(server, numFrames = numFrames, numChannels = numChannels, closeOnDisposal = true)
    res.alloc()
    res.record(path, fileType, sampleFormat)
    res
  }

  def fft(server: Server)(size: Int)(implicit tx: RT): Modifiable = {
    require(size >= 2 && isPowerOfTwo(size), "Must be a power of two and >= 2 : " + size)
    val res = create(server, numFrames = size, numChannels = 1)
    res.alloc()
    res
  }

  def apply(server: Server)(numFrames: Int, numChannels: Int = 1)(implicit tx: RT): Modifiable = {
    val res = create(server, numFrames = numFrames, numChannels = numChannels)
    res.alloc()
    res
  }

  def wrap(server: Server, peer: SBuffer, closeOnDisposal: Boolean = false)(implicit tx: RT): Modifiable = {
    require (server.peer == peer.server)
    val res = BufferImpl(server, peer)(numFrames = peer.numFrames, numChannels = peer.numChannels,
      closeOnDisposal = closeOnDisposal)
    res.mkOnline()
  }

  private def create(server: Server, numFrames: Int, numChannels: Int, closeOnDisposal: Boolean = false)
                    (implicit tx: RT): BufferImpl = {
    val id    = server.allocBuffer()
    val peer  = SBuffer(server.peer, id)
    BufferImpl(server, peer)(numFrames = numFrames, numChannels = numChannels, closeOnDisposal = closeOnDisposal)
  }

  trait Modifiable extends Buffer {
    def zero()(implicit tx: RT): Unit

    def fill(index: Int, num: Int, value: Float)(implicit tx: RT): Unit

    def setn(values: IndexedSeq[Float])(implicit tx: RT): Unit

    def setn(pairs: (Int, IndexedSeq[Float])*)(implicit tx: RT): Unit

    /** Issues a `/b_gen` command, for example preparing a wave table.
      *
      * Note that this must not be used where the command has dependencies, such as `PreparePartConv`
      * which depends on the impulse response input buffer! In that case use `addMessage` manually.
      */
    def gen(cmd: BufferGen.Command)(implicit tx: RT): Unit

    def read(path: String, fileStartFrame: Long = 0L, numFrames: Int = -1, bufStartFrame: Int = 0)
            (implicit tx: RT): Unit

    def readChannel(path: String, channels: Seq[Int], fileStartFrame: Long = 0L, numFrames: Int = -1,
                    bufStartFrame: Int = 0)(implicit tx: RT): Unit
  }
}

trait Buffer extends Resource {
  def peer: SBuffer
  def id: Int

  def numChannels: Int
  def numFrames  : Int

  def write(path: String, fileType: AudioFileType = AudioFileType.AIFF,
            sampleFormat: SampleFormat = SampleFormat.Float, numFrames: Int = -1, startFrame: Int = 0,
            leaveOpen: Boolean = false /* , completion: Optional[Packet] = None */)(implicit tx: RT): Unit
}