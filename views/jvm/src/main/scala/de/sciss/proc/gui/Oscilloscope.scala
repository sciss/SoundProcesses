/*
 *  Oscilloscope.scala
 *  (SoundProcesses)
 *
 *  Copyright (c) 2010-2025 Hanns Holger Rutz. All rights reserved.
 *
 *	This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.proc.gui

import de.sciss.lucre.swing.LucreSwing._
import de.sciss.lucre.synth.{RT, Server}
import de.sciss.proc.gui.impl.{ScopeBaseImpl, ScopePanelBaseImpl}
import de.sciss.synth.swing.j.JScopeView.Config
import de.sciss.synth.swing.j.{AbstractScopePanel, JScopeView, ScopeViewLike}
import de.sciss.synth.{Bus => SBus}

import javax.swing.{JComponent, JPanel}

object Oscilloscope {
  def apply(server: Server, channelStyle: Int = 0, bufSize: Int = 0, xZoom: Double = 1.0, yZoom: Double = 1.0,
            logAmp: Boolean = false, logAmpMin: Double = -84.0)
           (implicit tx: RT): Oscilloscope = {
    val res = new Impl(server)
    deferTx {
      res.initGUI(channelStyle = channelStyle, bufSize = bufSize, xZoom = xZoom, yZoom = yZoom,
        logAmp = logAmp, logAmpMin = logAmpMin)
    }
    res
  }

  private class PanelImpl(protected val scope: ScopeBaseImpl[PanelImpl]) extends JPanel with AbstractScopePanel[JScopeView]
    with ScopePanelBaseImpl[PanelImpl] {

    private[this] val _view = new JScopeView

    override def view: JScopeView = _view

    install(this)

    override def config: Config = view.config

    override def config_=(value: Config): Unit =
      view.config = value

    override def mkSynthGraphImpl(_bus: SBus): Unit =
      mkSynthGraph(_bus)
  }

  private final class Impl(server: Server) extends ScopeBaseImpl[PanelImpl](server) with Oscilloscope {
    override protected def mkPanel(): PanelImpl = new PanelImpl(this)

    override def component: JComponent with ScopeViewLike = panel
  }
}
trait Oscilloscope extends ScopeBase {
  /** The swing component showing the scope. Must be called on the EDT */
  override def component: JComponent with ScopeViewLike
}