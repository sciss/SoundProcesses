/*
 *  ScopeBase.scala
 *  (SoundProcesses)
 *
 *  Copyright (c) 2010-2025 Hanns Holger Rutz. All rights reserved.
 *
 *	This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.proc.gui

import de.sciss.lucre.Disposable
import de.sciss.lucre.synth.{Bus, Group, RT}
import de.sciss.synth.AddAction
import de.sciss.synth.swing.j.ScopeViewLike

import javax.swing.JComponent

trait ScopeBase extends Disposable[RT] {
  def bus(implicit tx: RT): Bus

  def bus_=(b: Bus)(implicit tx: RT): Unit

  def target(implicit tx: RT): Group

  def target_=(value: Group)(implicit tx: RT): Unit

  def addAction(implicit tx: RT): AddAction

  def addAction_=(value: AddAction)(implicit tx: RT): Unit

  def start()(implicit tx: RT): Unit
  def stop ()(implicit tx: RT): Unit

  def component: JComponent with ScopeViewLike
}
