/*
 *  Oscilloscope.scala
 *  (SoundProcesses)
 *
 *  Copyright (c) 2010-2025 Hanns Holger Rutz. All rights reserved.
 *
 *	This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.proc.gui

import de.sciss.lucre.swing.LucreSwing._
import de.sciss.lucre.synth.{RT, Server}
import de.sciss.proc.gui.impl.{ScopeBaseImpl, ScopePanelBaseImpl}
import de.sciss.synth.swing.j.JScopeView.Config
import de.sciss.synth.swing.j.{AbstractFreqScopePanel, FreqScopeViewLike, JFreqScopeView}
import de.sciss.synth.{Bus => SBus}

import javax.swing.{JComponent, JPanel}

object SpectrumAnalyzer {
  def apply(server: Server, channelStyle: Int = 0, bufSize: Int = 0, xZoom: Double = 1.0, yZoom: Double = 1.0,
            logAmp: Boolean = true, logAmpMin: Double = -84.0, xResolution: Double = 1.0,
            logFreq: Boolean = true, logFreqMin: Double = 30.0)
          (implicit tx: RT): SpectrumAnalyzer = {
    val res = new Impl(server)
    deferTx {
      res.initGUI(channelStyle = channelStyle, bufSize = bufSize, xZoom = xZoom, yZoom = yZoom,
        logAmp = logAmp, logAmpMin = logAmpMin)
      val c = res.component
      c.xResolution = xResolution.toFloat
      c.logFreq     = logFreq
      c.logFreqMin  = logFreqMin.toFloat
    }
    res
  }

  private class PanelImpl(protected val scope: ScopeBaseImpl[PanelImpl]) extends JPanel with AbstractFreqScopePanel
    with ScopePanelBaseImpl[PanelImpl] {

    private[this] val _view = new JFreqScopeView

    override def view: JFreqScopeView = _view

    install(this)

    override def config: Config = view.config

    override def config_=(value: Config): Unit =
      view.config = value

    override def mkSynthGraphImpl(_bus: SBus): Unit =
      mkSynthGraph(_bus)
  }

  private final class Impl(server: Server) extends ScopeBaseImpl[PanelImpl](server) with SpectrumAnalyzer {
    override protected def mkPanel(): PanelImpl = new PanelImpl(this)

    override def component: JComponent with FreqScopeViewLike = panel
  }
}
trait SpectrumAnalyzer extends ScopeBase {
  /** The swing component showing the scope. Must be called on the EDT */
  override def component: JComponent with FreqScopeViewLike
}