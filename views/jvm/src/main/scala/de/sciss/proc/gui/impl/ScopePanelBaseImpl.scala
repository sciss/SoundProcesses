/*
 *  ScopePanelBaseImpl.scala
 *  (SoundProcesses)
 *
 *  Copyright (c) 2010-2025 Hanns Holger Rutz. All rights reserved.
 *
 *	This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.proc.gui.impl

import de.sciss.lucre.synth.RT
import de.sciss.synth.swing.j.{JScopeView, ScopeViewLike}
import de.sciss.synth.{Bus => SBus}

import scala.concurrent.stm.atomic

trait ScopePanelBaseImpl[P <: ScopePanelBaseImpl[P]] extends ScopeViewLike {
  protected def scope: ScopeBaseImpl[P]

  var bufferSize: Int

  // Dotty bug
//  var bus: SBus
  def bus: SBus
  def bus_=(value: SBus): Unit

  def mkSynthGraphImpl(_bus: SBus): Unit

  // Dotty bug
//  var config: JScopeView.Config
  def config: JScopeView.Config
  def config_=(value: JScopeView.Config): Unit

  // used by subclasses
  protected def mkBusSynth(_bus: SBus): Unit = {
    atomic { itx =>
      implicit val tx: RT = RT.wrap(itx)
      scope.mkBusSynthTx(_bus)
    }
  }
}
