package de.sciss.proc

import de.sciss.lucre.Txn
import de.sciss.lucre.edit.UndoManager
import de.sciss.lucre.expr.LucreExpr
import de.sciss.lucre.swing.UndoRedo
import de.sciss.submin.Submin

import scala.swing.{Component, Frame, MainFrame, Menu, MenuBar, MenuItem, SimpleSwingApplication}

trait AppLike[T <: Txn[T]] extends SimpleSwingApplication {
  implicit lazy val undo: UndoManager[T] = UndoManager()

  def undoRedo: UndoRedo[T]

  override def main(args: Array[String]): Unit = {
    LucreExpr.init()
    Submin.install(false) // true
    //    WebLookAndFeel.install()
    super.main(args)
  }

  protected def mkView(): Component

  lazy val top: Frame = {
    val mb = new MenuBar {
      contents += new Menu("Edit") {
        contents += new MenuItem(undoRedo.undoAction)
        contents += new MenuItem(undoRedo.redoAction)
      }
    }

    val res = new MainFrame {
      title = "LucreSwing"
      contents = mkView()
      menuBar = mb
      pack().centerOnScreen()
      open()
    }
    res
  }
}
