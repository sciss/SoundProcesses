package de.sciss.proc.gui

import de.sciss.lucre.swing.LucreSwing.deferTx
import de.sciss.lucre.synth.{Bus, InMemory}
import de.sciss.proc.{AuralSystem, Universe}
import de.sciss.submin.Submin

import scala.swing.{Component, MainFrame}

object SpectrumAnalyzerTest {
  type S = InMemory
  type T = InMemory.Txn

  def main(args: Array[String]): Unit = {
    Submin.install(true)

    implicit val system: S = InMemory()
    implicit val universe: Universe[T] = system.step { implicit tx => Universe.dummy }

    system.step { implicit tx =>
      val as = universe.auralSystem
      as.start()
      as.reactNow { implicit tx => {
        case AuralSystem.Running(s) =>
          val view = SpectrumAnalyzer(s)
          view.bus = Bus.soundIn(s, 1)
          view.start()
          deferTx {
            new MainFrame {
              title = "Spectrum Analyzer"
              contents = Component.wrap(view.component)
              pack().centerOnScreen()
              open()
            }
            ()
          }
        case _ => ()
      }}
      ()
    }
  }
}
